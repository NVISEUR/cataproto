﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Game_data.Cats.Scripts
{
    /// <summary>
    /// Handle the behavior of a cat details object.
    /// <para>Those objects are used to show cat details when selecting them.</para>
    /// </summary>
    [DisallowMultipleComponent]
    public class CatDetails : MonoBehaviour, IPointerClickHandler
    {
        /// <summary>
        /// The text-name of the cat.
        /// </summary>
        [SerializeField] private TextMeshProUGUI m_catName;
        /// <summary>
        /// The text-attack of the cat.
        /// </summary>
        [SerializeField] private TextMeshProUGUI m_catAttack;
        /// <summary>
        /// The text-health of the cat.
        /// </summary>
        [SerializeField] private TextMeshProUGUI m_catHealth;
        /// <summary>
        /// The text-size of the cat.
        /// </summary>
        [SerializeField] private TextMeshProUGUI m_catSize;
        /// <summary>
        /// The text-ability of the cat.
        /// </summary>
        [SerializeField] private TextMeshProUGUI m_catAbility;

        /// <summary>
        /// Initialize the different texts from the given cat data.
        /// </summary>
        /// <param name="p_catData">The cat data we wish to show.</param>
        public void Initialize(CatData p_catData)
        {
            m_catName.text = p_catData.GetName();
            m_catAttack.text = p_catData.GetDamage().ToString();
            m_catHealth.text = p_catData.GetHealth().ToString();
            m_catSize.text = (Math.Abs(p_catData.GetScale() - 1) < float.Epsilon) ? "normal" : (p_catData.GetScale() > 1) ? "big" : "small";
            m_catAbility.text = p_catData.GetAbility().GetDescription();
        }

        /// <summary>
        /// On click, we deactivate this gameobject.
        /// </summary>
        /// <param name="eventData">The pointer event data.</param>
        public void OnPointerClick(PointerEventData eventData)
        {
            gameObject.SetActive(false);
        }
    }
}
