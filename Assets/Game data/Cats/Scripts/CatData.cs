﻿using System;
using Game_data.Abilities.Scripts;
using UnityEngine;

namespace Game_data.Cats.Scripts
{
	/// <summary>
	/// Scriptable object representing the data of a particular cat.
	/// </summary>
	[CreateAssetMenu(fileName = "Data", menuName = "Data/Cat", order = 1)]
	public class CatData : ScriptableObject
	{
		/// <summary>
		/// The id of the cat.
		/// <para>Should be unique, used to differentiate them when sending data between scene/players.</para>
		/// </summary>
		[SerializeField] private Byte m_id;
		/// <summary>
		/// The name of the cat.
		/// </summary>
		[SerializeField] private string m_name;
		/// <summary>
		/// The sprite used by the cat display.
		/// </summary>
		[SerializeField] private Sprite m_sprite;
		/// <summary>
		/// The maximum health of this cat.
		/// </summary>
		[SerializeField] private int m_health = 100;
		/// <summary>
		/// The base damages done by this cat.
		/// </summary>
		[SerializeField] private int m_damage = 25;
		/// <summary>
		/// The initiative of this cat.
		/// <para>An higher initiative means that it will play sooner.</para>
		/// </summary>
		[SerializeField] private int m_initiative = 3;
		/// <summary>
		/// The force used when throwing this cat.
		/// </summary>
		[SerializeField] private float m_force = 200;
		/// <summary>
		/// The weight of this cat.
		/// <para>It increase the cat mass and so slow it down.</para>
		/// </summary>
		[SerializeField] private float m_weight = 1;
		/// <summary>
		/// The scale of this cat.
		/// </summary>
		[Range(0.75f,1.75f)][SerializeField] private float m_scale = 1;
		/// <summary>
		/// The data of the special ability used by this cat.
		/// </summary>
		[SerializeField] private AbilityData m_ability = new AbilityData();

		public Byte GetId()
		{
			return m_id;
		}
		
		public string GetName()
		{
			return m_name;
		}

		public float GetForce()
		{
			return m_force;
		}
		
		public float GetWeight()
		{
			return m_weight;
		}
		
		public float GetScale()
		{
			return m_scale;
		}
		
		public int GetHealth()
		{
			return m_health;
		}
		
		public Sprite GetSprite()
		{
			return m_sprite;
		}
		
		public int GetInitiative()
		{
			return m_initiative;
		}
		
		public int GetDamage()
		{
			return m_damage;
		}
		
		public AbilityData GetAbility()
 		{
 			return m_ability;
 		}
 	}
 }