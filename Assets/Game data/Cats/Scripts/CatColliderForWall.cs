﻿using Game_data.Abilities.Scripts;
using UnityEngine;

namespace Game_data.Cats.Scripts
{
	/// <summary>
	/// Handle collision with walls.
	/// <para>It is separated from the cat script because the cat need two colliders, one for object it may ignore and one for the others.</para>
	/// </summary>
	[RequireComponent(typeof(Collider2D))]
	[DisallowMultipleComponent]
	public class CatColliderForWall : MonoBehaviour
	{
		/// <summary>
		/// The ability controller.
		/// <para>Needed to trigger onCollision events with the wall.</para>
		/// </summary>
		private AbilityController m_abilityController;
		/// <summary>
		/// The game controller.
		/// <para>Needed to check if it is this cat turn and it can trigger it's ability.</para>
		/// </summary>
		private CatManager m_catManager;
		/// <summary>
		/// The parent gameobject (The cat)
		/// <para>Needed to check whether or not this cat is the cat currently playing.</para>
		/// </summary>
		private Cat m_cat;

		public void SetCatManager(CatManager p_catManager)
		{
			m_catManager = p_catManager;
		}

		public void SetAbilityController(AbilityController p_abilityController)
		{
			m_abilityController = p_abilityController;
		}

		public void SetParent(Cat p_cat)
		{
			m_cat = p_cat;
		}
	
		/// <summary>
		/// Call the ability controller to launch eventual ability which trigger on wall collision.
		/// </summary>
		/// <param name="other">The collider of the object this cat is colliding with.</param>
		private void OnCollisionEnter2D(Collision2D other)
		{
			m_abilityController.OnCollision(other.gameObject.layer,m_catManager.GetPlayingCat().GetComponent<Cat>().Equals(m_cat));
		}
	}
}
