﻿using System.Linq;
using Scenes.Cat_management.Scripts;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Game_data.Cats.Scripts
{
    /// <summary>
    /// Handle the cat select boxes in the cat select screen.
    /// </summary>
    [RequireComponent(typeof(RectTransform))]
    [DisallowMultipleComponent]
    public class CatSelect : MonoBehaviour, IDragHandler, IEndDragHandler, IPointerClickHandler
    {
        /// <summary>
        /// The selection manager.
        /// <para>Called to select/unselect the cats</para>
        /// </summary>
        private MenuController m_menuController;
        /// <summary>
        /// The cat's data to display in the box.
        /// </summary>
        private CatData m_catData;
        /// <summary>
        /// The rect transform of this object.
        /// </summary>
        private RectTransform m_transform;
        /// <summary>
        /// The rect transform of selection square.
        /// <para>Used to see if we drop the box in the square or not.</para>
        /// </summary>
        private RectTransform[] m_catRects;
        /// <summary>
        /// The rect transform of the scroll rect section.
        /// <para>Used to parent the box.</para>
        /// </summary>
        private RectTransform m_scrollRectTransform;
        /// <summary>
        /// The rect transform of the canvas.
        /// <para>Used to parent the box.</para>
        /// </summary>
        private RectTransform m_canvas;
        /// <summary>
        /// Whether or not the box can be dragged.
        /// </summary>
        private bool m_canBeDragged;
        /// <summary>
        /// The cat detail object.
        /// <para>Used to show the details of the cat when clicking on the box.</para>
        /// </summary>
        private CatDetails m_catDetails;
        /// <summary>
        /// Whether or not the cat is currently being dragged
        /// </summary>
        private bool dragging;
        /// <summary>
        /// The slot this cat is in.
        /// </summary>
        private int m_slot = -1;
        
        /// <summary>
        /// Initialize the box and all it's members.
        /// </summary>
        /// <param name="p_selectionManager">The selection manager.</param>
        /// <param name="p_CatData">The cat data.</param>
        /// <param name="p_catRects">The cat slots transforms.</param>
        /// <param name="p_canvas">The canvas on the scene.</param>
        /// <param name="p_canBeDragged">Whether or not the box can be dragged.</param>
        /// <param name="p_catDetails">The cat details object.</param>
        public void Initialize(MenuController p_selectionManager, CatData p_CatData, RectTransform[] p_catRects
            , RectTransform p_canvas, bool p_canBeDragged,CatDetails p_catDetails)
        {
            m_catDetails = p_catDetails;
            m_canBeDragged = p_canBeDragged;
            m_menuController = p_selectionManager;
            m_catData = p_CatData;
            //Set the cat details sprite to the one in the catdata scriptableobject
            GetComponentsInChildren<RawImage>()
                .Where(go => go.gameObject != gameObject).ToArray()[0].texture = m_catData.GetSprite().texture;
            //Set the cat details name to the cat one
            GetComponentInChildren<TextMeshProUGUI>().text = m_catData.GetName();
            m_transform = GetComponent<RectTransform>();
            m_scrollRectTransform = m_transform.parent as RectTransform;
            m_catRects = p_catRects;
            m_canvas = p_canvas;
        }

        //On drag, change the cat details parent to the canvas and move it each frame to the mouse position
        public void OnDrag(PointerEventData eventData)
        {
            if (!m_canBeDragged)
                return;
            
            dragging = true;
            
            if(m_transform.parent != m_canvas)
                m_transform.SetParent(m_canvas,true);
            
            m_transform.position = Input.mousePosition;
        }


        //If the cat details is dropped in the selected section, add it to the selected cats if possible
        //If it was added, change the parent to the selected section.
        //If the cat details is dropped anywhere else, reset it's parent to the scroll rect
        //And unselect it if it was selected
        public void OnEndDrag(PointerEventData eventData)
        {
            if (!m_canBeDragged)
                return;
            
            dragging = false;

            for (int i = 0; i < m_catRects.Length; i++)
            {
                if (RectTransformUtility.RectangleContainsScreenPoint(m_catRects[i],Input.mousePosition))
                {
                    bool l_selected = m_menuController.SelectCat(m_catData,i,gameObject);
                    if (l_selected)
                    {
                        m_slot = i;
                    }
                    else
                        m_transform.SetParent(m_scrollRectTransform);
                    
                    return;
                }
            }
        
            m_menuController.UnselectCat(m_catData, m_slot, gameObject);
            m_slot = -1;
        }

        public int GetSlot()
        {
            return m_slot;
        }

        public void SetSlot(int p_slot)
        {
            m_slot = p_slot;
        }

        public void Unselect()
        {
            m_transform.SetParent(m_scrollRectTransform);
            m_transform.localPosition = Vector3.zero;
        }

        //If we click on the cat and we are not dragging it, show it's details
        public void OnPointerClick(PointerEventData eventData)
        {
            if (dragging)
                return;
            
            //Show the cat details
            m_catDetails.Initialize(m_catData);
            m_catDetails.gameObject.SetActive(true);
        }
    }
}
