﻿using System.Collections.Generic;
using System.Linq;
using Game_data.Items.Scripts;
using Scenes.General.Scripts;
using UnityEngine;
using UnityEngine.UI;

namespace Game_data.Cats.Scripts
{
    /// <summary>
    /// Manage the cats during the game.
    /// </summary>
    [DisallowMultipleComponent]
    public class CatManager : MonoBehaviour
    {
        /// <summary>
        /// The cat prefab.
        /// <para>Used to instantiate cats when the game starts.</para>
        /// </summary>
        [SerializeField] private GameObject m_catPrefab;
        /// <summary>
        /// The transform of the parent in the hierarchy.
        /// </summary>
        [SerializeField] private Transform m_catsParent;
        /// <summary>
        /// The data of the cats.
        /// </summary>
        [SerializeField] private CatData[] m_catsData;
        /// <summary>
        /// The positions of the player one's cats.
        /// </summary>
        [SerializeField] private Vector3[] m_playerOnePos;
        /// <summary>
        /// The positions of the player two's cats.
        /// </summary>
        [SerializeField] private Vector3[] m_playerTwoPos;
        /// <summary>
        /// Canvas scroll rect.
        /// <para>Used to store the cats order.</para>
        /// </summary>
        [SerializeField] private RectTransform m_scrollrect;
        /// <summary>
        /// The cat initiative prefab.
        /// <para>Used to show the cats order between turn</para>
        /// </summary>
        [SerializeField] private GameObject m_initObj;
        /// <summary>
        /// The array containing the player one's cat.
        /// </summary>
        private Cat[] m_playerOneCats = new Cat[MAX_CATS_BY_TEAM];
        /// <summary>
        /// The array containing the player two's cat.
        /// </summary>
        private Cat[] m_playerTwoCats = new Cat[MAX_CATS_BY_TEAM];
        /// <summary>
        /// The list of all the cats rigidbody.
        /// <para>Used to check if any cat is moving.</para>
        /// </summary>
        private Rigidbody2D[] m_catsRb = new Rigidbody2D[MAX_CATS_BY_TEAM*2];
        /// <summary>
        /// The maximum number of cats each team can have.
        /// </summary>
        private const int MAX_CATS_BY_TEAM = 4;
        /// <summary>
        /// The data manager used to get the selected cats IDs.
        /// </summary>
        private DataManager m_dataManager;
        /// <summary>
        /// The index of the last cat played by the player one.
        /// </summary>
        private int m_playerOneCatIndex = -1;
        /// <summary>
        /// The index of the last cat played by the player two.
        /// </summary>
        private int m_playerTwoCatIndex = -1;
        /// <summary>
        /// The threshold before which the cat is considered not moving.
        /// </summary>
        private const float THRESHOLD = 0.005f;
        /// <summary>
        /// The currently playing cat.
        /// </summary>
        private GameObject m_playingCat;
        /// <summary>
        /// The color of the player one's cat's outline.
        /// </summary>
        private static readonly Color m_p1Color = new Color(.2f, .3f, .8f);	
        /// <summary>
        /// The color of the player two's cat's outline.
        /// </summary>
        private static readonly Color m_p2Color = new Color(.6f, .2f, .2f);
        private void Awake()
        {
            //Get the data manager
            m_dataManager = FindObjectOfType<DataManager>();
            //Get the selected cats array
            byte[] l_selection = m_dataManager.GetSelectedCats();

            //Instantiate the player one cats
            for (int i = 0; i < MAX_CATS_BY_TEAM; i++)
            {
                for (int j = 0; j < m_catsData.Length; j++)
                {
                    if (l_selection[i] == m_catsData[j].GetId())
                    {
                        //Player 1
                        //Instantiate the player cat
                        Cat l_cat = Instantiate(m_catPrefab, m_playerOnePos[i],Quaternion.identity, m_catsParent).GetComponent<Cat>();
                        //Initialize the cat with it's data and player number
                        l_cat.Initialize(m_catsData[j],1, this,new Item[]{});
                        //Add the cat to the cats array
                        m_playerOneCats[i] = l_cat;
                        
                        //Player 2
                        //Instantiate the player cat
                        l_cat = Instantiate(m_catPrefab, m_playerTwoPos[i],Quaternion.identity,m_catsParent).GetComponent<Cat>();
                        //Initialize the cat with it's data and player number
                        l_cat.Initialize(m_catsData[i],2, this,new Item[]{});
                        //Add the cat to the cats array
                        m_playerTwoCats[i] = l_cat;
                    }
                }
            }

            //Order the two lists based on the cats initiative, then reverse it (higher first)
            m_playerOneCats = m_playerOneCats
                .ToList()
                .OrderBy(cat => cat.getData().GetInitiative())
                .Reverse()
                .ToArray();
            m_playerTwoCats = m_playerTwoCats
                .ToList()
                .OrderBy(cat => cat.getData().GetInitiative())
                .Reverse()
                .ToArray();

            int l_p1 = 0;
            int l_p2 = 0;
            GameObject l_ini;
            
            //Create the initiative preview
            for (int i = 0; i < MAX_CATS_BY_TEAM*2; i++)
            {
                if (i % 2 == 0)
                {
                    l_ini = Instantiate(m_initObj, m_scrollrect);
                    RawImage l_rend = l_ini.GetComponent<RawImage>();
                    l_rend.color = m_p1Color;
                    l_rend.texture = m_playerOneCats[l_p1].getData().GetSprite().texture;
                    l_p1++;
                }
                else
                {
                    l_ini = Instantiate(m_initObj, m_scrollrect);
                    RawImage l_rend = l_ini.GetComponent<RawImage>();
                    l_rend.color = m_p2Color;
                    l_rend.texture = m_playerOneCats[l_p2].getData().GetSprite().texture;
                    l_p2++;
                }
            }
            
            //Create a list with all the rigidbody2d used when verifying if any cat is moving
            for (int i = 0; i < MAX_CATS_BY_TEAM*2; i++)
            {
                m_catsRb[i] = i < MAX_CATS_BY_TEAM ? 
                    m_playerOneCats[i].GetComponent<Rigidbody2D>() : m_playerTwoCats[i - MAX_CATS_BY_TEAM].GetComponent<Rigidbody2D>();
            }
        }

        /// <summary>
        /// Return whether or not any of the cats is moving.
        /// </summary>
        /// <returns>True if any of the cats is moving.</returns>
        public bool IsAnyCatMoving()
        {
            for (int i = 0; i < m_catsRb.Length; i++)
            {
                //Check if the squared magnitude of the velocity is inferior to the threshold or not
                if (m_catsRb[i].velocity.sqrMagnitude > THRESHOLD)
                {
                    return true;
                }
                m_catsRb[i].velocity = Vector3.zero;
            }
			
            //If all the cats rigidbodies are sleeping, we can go to the next turn
            return false;
        }

        /// <summary>
        /// Select the next cat for the new turn.
        /// </summary>
        /// <param name="p_playerTurn">The turn of the current player.</param>
        public void SelectNextCat(int p_playerTurn)
        {
            Cat l_catComp;

            if (p_playerTurn == 1)
            {
                //Get the cat from the list
                do
                {
                    //Then increment the index
                    m_playerOneCatIndex++;
                    //Set the player cat index to 0 if needed, or to the actual cat index
                    m_playerOneCatIndex = (m_playerOneCatIndex == m_playerOneCats.Length) ? 0 : m_playerOneCatIndex;

                    l_catComp = m_playerOneCats[m_playerOneCatIndex];

                    if (l_catComp.IsStunned())
                    {
                        l_catComp.Regen();
                    }
                } while (l_catComp.IsStunned());
                
            }
            else
            {
                //Get the cat from the list
                do
                {
                    //Then increment the index
                    m_playerTwoCatIndex++;
                    //Set the player cat index to 0 if needed, or to the actual cat index
                    m_playerTwoCatIndex = (m_playerTwoCatIndex == m_playerTwoCats.Length) ? 0 : m_playerTwoCatIndex;

                    l_catComp = m_playerTwoCats[m_playerTwoCatIndex];
                    
                    if (l_catComp.IsStunned())
                    {
                        l_catComp.Regen();
                    }
                } while (l_catComp.IsStunned());
            }
            m_playingCat = l_catComp.gameObject;
        }

        /// <summary>
        /// Draw the cat position in the scene to ease with level design.
        /// </summary>
        private void OnDrawGizmos()
        {
            for (int i = 0; i < m_playerOnePos.Length; i++)
            {
                Gizmos.DrawSphere(m_playerOnePos[i],.1f);
            }
            
            for (int i = 0; i < m_playerTwoPos.Length; i++)
            {
                Gizmos.DrawSphere(m_playerTwoPos[i],.1f);
            }
        }

        public GameObject GetPlayingCat()
        {
            return m_playingCat;
        }
    }
}
