﻿using System;
using Game_data.Abilities.Scripts;
using Game_data.Health_System.Scripts;
using Game_data.Items.Scripts;
using Ressources.Tools.ObjectPooling;
using Scenes.Game_scene.Script;
using UnityEngine;

namespace Game_data.Cats.Scripts
{
	/// <summary>
	/// Handle the cat along it's lifetime.
	/// </summary>
	[RequireComponent(typeof(Rigidbody2D))]
	[RequireComponent(typeof(SpriteRenderer))]
	[RequireComponent(typeof(Health))]
	[RequireComponent(typeof(Collider2D))]
	[DisallowMultipleComponent]
	public class Cat : MonoBehaviour
	{
		/// <summary>
		/// The rigidbody of the cat.
		/// <para>Used to throw the cat and change it's mass.</para>
		/// </summary>
		[SerializeField] private Rigidbody2D m_rigidBody;
		/// <summary>
		/// The sprite renderer of the cat.
		/// <para>Used to change the it to the team color at start.</para>
		/// </summary>
		[SerializeField] private SpriteRenderer m_spriteRenderer;
		/// <summary>
		/// The game controller.
		/// <para>Needed to know which cat is playing and to increase the scores.</para>
		/// </summary>
		[SerializeField] private GameController m_gameController;
		/// <summary>
		/// The object pooler.
		/// <para>Needed to be able to pass it to the ability controller</para>
		/// </summary>
		[SerializeField] private ObjectPooler m_objectPooler;
		/// <summary>
		/// The health component of the cat.
		/// <para>Needed to heal the cat what it is stun.</para>
		/// </summary>
		[SerializeField] private Health m_health;
		/// <summary>
		/// The sprite renderer of the cat display.
		/// <para>Needed to change it's color during the game.</para>
		/// </summary>
		[SerializeField] private SpriteRenderer m_catDisplay;
		/// <summary>
		/// The player number.
		/// <para>Used to identify which player this cat belong to.</para>
		/// </summary>
		private int m_playerNumber;
		/// <summary>
		/// The Scriptable object containing this cat data.
		/// </summary>
		private CatData m_catData;
		/// <summary>
		/// The ability controller.
		/// <para>Needed to launch the abilities when needed.</para>
		/// </summary>
		private AbilityController m_abilityController;
		/// <summary>
		/// The color of the player one's cat's outline.
		/// </summary>
		private static readonly Color m_p1Color = new Color(.2f, .3f, .8f);	
		/// <summary>
		/// The color of the player two's cat's outline.
		/// </summary>
		private static readonly Color m_p2Color = new Color(.6f, .2f, .2f);
		/// <summary>
		/// The color of cat display when it's stunned.
		/// </summary>
		private static readonly Color m_stunColor = new Color(.3f, .3f, .3f);
		/// <summary>
		/// Whether or not is the cat stunned.
		/// </summary>
		private bool m_stun;
		/// <summary>
		/// Whether or not has the cat been thrown this turn.
		/// </summary>
		private bool m_thrown;
		/// <summary>
		/// The number of frame during which the cat has not moved.
		/// </summary>
		private int m_frameWithoutMovement;
		/// <summary>
		/// The threshold below which the cat is considered not moving.
		/// </summary>
		private const float VELOCITY_THRESHOLD = 0.005f;
		/// <summary>
		/// The cats manager.
		/// <para>Used to set the wall collider and get the cat that is playing</para>
		/// </summary>
		private CatManager m_catManager;

		private float m_force;
		private float m_damage;

		/// <summary>
		/// Initialize the cat using the provided cat data scriptable object and the player number.
		/// </summary>
		/// <param name="p_catData">The data of this cat.</param>
		/// <param name="p_playerNumber">The player which this cat belong to.</param>
		/// <param name="p_catManager">The cat manager.</param>
		public void Initialize(CatData p_catData, int p_playerNumber, CatManager p_catManager, Item[] p_items)
		{
			if(m_rigidBody == null)
				m_rigidBody = GetComponent<Rigidbody2D>();
			if (m_spriteRenderer == null)
				m_spriteRenderer = GetComponent<SpriteRenderer>();
			if (m_gameController == null)
				m_gameController = FindObjectOfType<GameController>();
			if (m_objectPooler == null)
				m_objectPooler = FindObjectOfType<ObjectPooler>();
			if (m_health == null)
				m_health = GetComponent<Health>();

			m_catManager = p_catManager;
			
			//Get the transform
			Transform l_transform = transform;
			//Set the cat data and player number
			m_catData = p_catData;
			m_playerNumber = p_playerNumber;
			//Change the cat color based on the player number
			m_spriteRenderer.color = (p_playerNumber == 1) ? m_p1Color : m_p2Color;
			
			//Create this cat abilityController, using the data from the cat data scriptable object, and passing
			//The collider, transform and player number
			m_abilityController = new AbilityController(m_catData.GetAbility(), GetComponent<CircleCollider2D>(), l_transform, m_objectPooler,m_playerNumber, m_gameController);
			
			//Set the sprite based on the data from the scriptable object
			m_catDisplay.sprite = m_catData.GetSprite();

			//Set the mass according to the weight of the cat
			m_rigidBody.mass = m_catData.GetWeight();
			
			//Set the ability controller of the children script for him to handle collision whith walls
			CatColliderForWall l_catColliderForWall = GetComponentInChildren<CatColliderForWall>();
			l_catColliderForWall.SetCatManager(m_catManager);
			l_catColliderForWall.SetAbilityController(m_abilityController);
			l_catColliderForWall.SetParent(this);

			m_force = p_catData.GetForce();
			m_damage = p_catData.GetDamage();
			//Set the max health of the cat based on the cat data
			int l_health = m_catData.GetHealth();
			Vector3 l_sizes = new Vector3(m_catData.GetScale(),m_catData.GetScale(),1);

			for (int i = 0; i < p_items.Length; i++)
			{
				ItemEffect l_effectOne = null;
				ItemEffect l_effectTwo = null;
				if (p_items[i].GetFirstEffect().GetValue() != 0)
				{
					l_effectOne = p_items[i].GetFirstEffect();
					switch (l_effectOne.GetType())
					{
						case ItemEffectType.ChangeDamage:
							m_damage += l_effectOne.GetValue();
							break;
						case ItemEffectType.ChangeHealth:
							l_health += (int)l_effectOne.GetValue();
							break;
						case ItemEffectType.ChangeSize:
							l_sizes.x += l_effectOne.GetValue();
							l_sizes.y += l_effectOne.GetValue();
							break;
						case ItemEffectType.ChangeForce:
							m_force += l_effectOne.GetValue();
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
				}

				if (p_items[i].GetSecondEffect().GetValue() != 0)
				{
					l_effectTwo = p_items[i].GetFirstEffect();
					switch (l_effectTwo.GetType())
					{
						case ItemEffectType.ChangeDamage:
							m_damage += l_effectTwo.GetValue();
							break;
						case ItemEffectType.ChangeHealth:
							l_health += (int)l_effectTwo.GetValue();
							break;
						case ItemEffectType.ChangeSize:
							l_sizes.x += l_effectTwo.GetValue();
							l_sizes.y += l_effectTwo.GetValue();
							break;
						case ItemEffectType.ChangeForce:
							m_force += l_effectTwo.GetValue();
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}
				}
			}
			GetComponent<Health>().SetMaxHealth(l_health);
			//Rescale the pawn based on the data from the scriptable object
			l_transform.localScale = l_sizes;
		}

		private void Update()
		{
			//If this cat is the one playing and has been thrown
			if (m_catManager.GetPlayingCat() == gameObject && m_thrown)
			{
				if (m_rigidBody.velocity.magnitude < VELOCITY_THRESHOLD)
				{
					m_frameWithoutMovement++;
					//If the cat had a velocity inferior to the threshold for 3 frame or more
					if (m_frameWithoutMovement >= 3)
					{
						//Call the onstop ability
						OnStop();
					}
				}
			}
		}

		/// <summary>
		/// Return the cat data.
		/// </summary>
		/// <returns>The cat data.</returns>
		public CatData getData()
		{
			return m_catData;
		}

		/// <summary>
		/// Add a force in the given direction multiplied by the force from the cat data.
		/// </summary>
		/// <param name="p_direction">The normalized direction in which the cat should be thrown.</param>
		public void Throw(Vector3 p_direction)
		{
			m_rigidBody.AddForce(p_direction*m_force);
			m_thrown = true;
		}
		
		private void OnCollisionEnter2D(Collision2D other)
		{
			HandleCollision(other.gameObject);
		}

		private void OnTriggerEnter2D(Collider2D other)
		{
			HandleCollision(other.gameObject);
		}

		/// <summary>
		/// Handle collisions with other objects.
		/// </summary>
		/// <param name="other">The collider of the other object.</param>
		private void HandleCollision(GameObject other)
		{
			//Nothing special should be done when it is not our turn or we are stunned
			if (m_playerNumber != m_catManager.GetPlayingCat().GetComponent<Cat>().getTeam()
			    || m_stun
			    || (other.layer != LayerMask.NameToLayer("Cat") && other.layer != LayerMask.NameToLayer("Damageable")))
				return;

			if (other.layer == LayerMask.NameToLayer("Cat"))
			{
				//Get the cat component of the other cat
				Cat l_cat = other.GetComponent<Cat>();
				
				//If the cat is an ally, do not do damage
				if (l_cat.m_playerNumber == m_playerNumber || l_cat.m_stun)
					return;
			}
			
			Health l_health = other.GetComponent<Health>();
			
			//Call the on collision ability with the cats if it collide with an enemy if it is our turn
			bool l_isCatTurn = m_catManager.GetPlayingCat().GetComponent<Cat>().Equals(this);
			m_abilityController.OnCollision(other.layer,l_isCatTurn);

			//Deal damage to the other cat based on the base damage and the ability damage multiplier
			int l_damage = Mathf.FloorToInt(m_damage * m_abilityController.GetDamageMultiplier());

			DamageType l_type = Math.Abs(m_abilityController.GetDamageMultiplier() - 1) < float.Epsilon ? DamageType.normal : DamageType.ability;
			ushort l_points = l_health.DealDamage(l_damage,l_type);
			
			if(l_points > 0)
				m_gameController.IncreaseScore(m_playerNumber,l_points);
		}

		/// <summary>
		/// Call the OnStop ability if any.
		/// </summary>
		public void OnStop()
		{
			m_abilityController.OnStop();
			m_frameWithoutMovement = 0;
		}

		/// <summary>
		/// Call the OnStart ability if any.
		/// </summary>
		public void OnStart()
		{
			m_abilityController.OnStart();
			m_thrown = false;
		}

		/// <summary>
		/// Return the team of the cat
		/// </summary>
		/// <returns>The team which this cat belong to.</returns>
		public int getTeam()
		{
			return m_playerNumber;
		}

		/// <summary>
		/// Change the color of the cat display.
		/// </summary>
		/// <param name="p_color">The new color to dye the cat with.</param>
		public void ChangeColor(Color p_color)
		{
			m_catDisplay.color = p_color;
		}

		/// <summary>
		/// Reset the color of the cat display.
		/// <para>The color will be different whether it is stunned or not.</para>
		/// </summary>
		public void ResetColor()
		{
			if (m_stun)
				m_catDisplay.color = m_stunColor;
			else
				m_catDisplay.color = Color.white;

		}

		/// <summary>
		/// Stun the cat and change his color to the stun color.
		/// </summary>
		public void Stun()
		{
			m_stun = true;
			m_catDisplay.color = m_stunColor;
		}

		/// <summary>
		/// Heal 50% of the cat health and remove the stun if the cat is fully healed.
		/// </summary>
		public void Regen()
		{
			bool l_fullyHealed = m_health.Heal(50);

			if (l_fullyHealed)
			{
				m_stun = false;
				m_catDisplay.color = Color.white;
			}
		}

		/// <summary>
		/// Return true if the cat is stunned.
		/// </summary>
		/// <returns>Whether or not the cat is stunned</returns>
		public bool IsStunned()
		{
			return m_stun;
		}
	}
}
