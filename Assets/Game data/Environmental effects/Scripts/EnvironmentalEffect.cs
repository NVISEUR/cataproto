﻿using Game_data.Health_System.Scripts;
using Scenes.Game_scene.Script;
using UnityEngine;

namespace Game_data.Environmental_effects.Scripts
{
	/// <summary>
	/// Handle the lifetime of an environmental effect.
	/// </summary>
	[DisallowMultipleComponent]
	[RequireComponent(typeof(Collider2D))]
	public class EnvironmentalEffect : MonoBehaviour 
	{
		/// <summary>
		/// The turn controller.
		/// <para>Used to subscribe to the new turn event and know when a new turn start.</para>
		/// </summary>
		[SerializeField] private GameController m_gameController;
		/// <summary>
		/// The collider of the effect.
		/// <para>Used to calculate the radius of the effect when casting a circle.</para>
		/// </summary>
		[SerializeField] private CircleCollider2D m_collider;
		/// <summary>
		/// The array of result from the <see cref="Physics2D.OverlapCircleNonAlloc(Vector2,float,Collider2D[],int)"/>.
		/// </summary>
		private readonly Collider2D[] m_circleCastColliders = new Collider2D[8];
		/// <summary>
		/// The transform of the effect.
		/// <para>Needed for the position and radius.</para>
		/// </summary>
		private Transform m_transform;
		/// <summary>
		/// The effect data
		/// </summary>
		private EffectData m_data;
		/// <summary>
		/// The number of turn the effect should be active.
		/// </summary>
		private ushort m_nbTurn;

		private void Start()
		{
			if (m_gameController == null)
				m_gameController = FindObjectOfType<GameController>();
			if (m_collider == null)
				m_collider = GetComponent<CircleCollider2D>();
			//Subscribe to the new turn event to do the effect
			m_gameController.SubscribeToNewTurnEvent(OnNewTurn);
			m_transform = transform;
		}

		public void SetData(EffectData p_data)
		{
			m_data = p_data;
			m_nbTurn = m_data.GetNbTurn();
			GetComponent<SpriteRenderer>().color = m_data.GetColor();
		}

		/// <summary>
		/// Callback for the new turn event.
		/// </summary>
		public void OnNewTurn()
		{
			if (!gameObject.activeSelf)
				return;
			
			//reduce the number of turn remaining
			m_nbTurn--;
			
			//If the number of turn is 0, destroy the effect
			if(m_nbTurn == 0)
				gameObject.SetActive(false);
			else
			{
				//First call a OverlapCircleNonAlloc to get all the cats in the vicinity, using the cat transform, and the ability radius.
				Vector3 l_pos = m_transform.position;
				//Get all the cat in the area of effect
				int l_nbColl = Physics2D.OverlapCircleNonAlloc(l_pos, m_transform.localScale.x*m_collider.radius,m_circleCastColliders,1<<LayerMask.NameToLayer("Cat"));
				//If there is no cat, return
				if (l_nbColl == 0)
					return;
            
				//For each collisions
				for (int i = 0; i < m_circleCastColliders.Length ; i++)
				{
					if(m_circleCastColliders[i] == null || m_circleCastColliders[i].transform == m_transform)
						continue;

					//Deal damages if it is the needed effect
					if (m_data.GetType() == SpecialEffectType.damage)
					{
						Health l_catHealth = m_circleCastColliders[i].gameObject.GetComponent<Health>();
                        
						l_catHealth.DealDamage(Mathf.FloorToInt(m_data.GetValue()),DamageType.environmental);
					}
				}
			}
		}

		//If the effect is either slow or slide, the effect should be constant when in collision
		//So we set it on enter, and revert on exit
		private void OnTriggerEnter2D(Collider2D other)
		{
			if (m_data.GetType() == SpecialEffectType.slow)
			{
				other.gameObject.GetComponent<Rigidbody2D>().drag = 5f;
			}
			else if(m_data.GetType() == SpecialEffectType.slide)
			{
				other.gameObject.GetComponent<Rigidbody2D>().drag = 0f;
			}
		}

		private void OnTriggerExit2D(Collider2D other)
		{			
			if (m_data.GetType() == SpecialEffectType.slow || m_data.GetType() == SpecialEffectType.slide)
			{
				other.gameObject.GetComponent<Rigidbody2D>().drag = 0.75f;
			}
		}
	}
}