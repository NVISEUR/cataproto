using UnityEngine;

namespace Game_data.Environmental_effects.Scripts
{
	/// <summary>
	/// The possible types of special effects.
	/// </summary>
	public enum SpecialEffectType
	{
		//Deal damage to the cat each turn
		damage,
		//Decrease the rigidbody drag of the cat
		slide,
		//Increase the rigidbody drag of the cat
		slow
	}
	
	/// <summary>
	/// Scriptable object representing the data of a particular cat.
	/// </summary>
	[CreateAssetMenu(fileName = "Effect", menuName = "Data/Effect", order = 2)]
	public class EffectData : ScriptableObject
	{
		/// <summary>
		/// The type of the effect.
		/// </summary>
		[SerializeField] private SpecialEffectType m_type;
		/// <summary>
		/// The value of the effect.
		/// <para>Used by the damage type effects.</para>
		/// </summary>
		[SerializeField] private ushort m_value;
		/// <summary>
		/// The number of turn the effect should be active.
		/// </summary>
		[SerializeField] private ushort m_nbTurn;
		/// <summary>
		/// The color of the effect
		/// </summary>
		[SerializeField] private Color m_color;

		public new SpecialEffectType GetType()
		{
			return m_type;
		}

		public ushort GetValue()
		{
			return m_value;
		}

		public ushort GetNbTurn()
		{
			return m_nbTurn;
		}

		public Color GetColor()
		{
			return m_color;
		}
 	}
}