﻿using Game_data.Cats.Scripts;
using Game_data.Health_System.Scripts;
using Ressources.Tools.ObjectPooling;
using Scenes.Game_scene.Script;
using UnityEngine;

namespace Game_data.Abilities.Scripts
{
    /// <summary>
    /// Handle the execution of the abilities of a cat
    /// </summary>
    public class AbilityController
    {
        /// <summary>
        /// The ability data.
        /// </summary>
        private readonly AbilityData m_ability;
        /// <summary>
        /// The collider of the cat.
        /// <para>Needed for the ghost abilities.</para>
        /// </summary>
        private readonly Collider2D m_collider;
        /// <summary>
        /// The transform of the cat.
        /// <para>Needed for the throw and zone abilities.</para>
        /// </summary>
        private readonly Transform m_transform;
        /// <summary>
        /// Tha cat team.
        /// <para>Used by the throw and zone abilities to only select enemies.</para>
        /// </summary>
        private readonly int m_team;
        /// <summary>
        /// The array of result from the <see cref="Physics2D.OverlapCircleNonAlloc(Vector2,float,Collider2D[],int)"/>.
        /// <para>Needed for the throw and zone abilities.</para>
        /// </summary>
        private readonly Collider2D[] m_circleCastColliders = new Collider2D[8];
        /// <summary>
        /// The game controller.
        /// <para>Used to increase team scores if needed.</para>
        /// </summary>
        private readonly GameController m_gameController;
        /// <summary>
        /// The object pooler.
        /// <para>Used to get the pooled projectiles.</para>
        /// </summary>
        private readonly ObjectPooler m_objectPooler;
        /// <summary>
        /// The current damage multiplier of the cat.
        /// </summary>
        private float m_currentMultiplier = 1;
        /// <summary>
        /// Whether or not this is the first time the cat trigger a collision this turn.
        /// </summary>
        private bool m_firstColl = true;
        /// <summary>
        /// Whether or not has the OnStop ability been called this turn.
        /// </summary>
        private bool m_onStopCalled;

        /// <summary>
        /// The constructor.
        /// <para>Used to initialize all the members of the controller.</para>
        /// </summary>
        /// <param name="p_Ability">The ability data of the cat.</param>
        /// <param name="p_collider">The collider of the cat.</param>
        /// <param name="p_transform">The transform of the cat.</param>
        /// <param name="p_objectPooler">The object pooler.</param>
        /// <param name="p_team">The team of the cat.</param>
        /// <param name="p_gameController">The game controller.</param>
        public AbilityController(AbilityData p_Ability, Collider2D p_collider, Transform p_transform
            ,ObjectPooler p_objectPooler , int p_team, GameController p_gameController)
        {
            m_ability = p_Ability;
            m_collider = p_collider;
            m_transform = p_transform;
            m_team = p_team;
            m_objectPooler = p_objectPooler;
            m_gameController = p_gameController;
        }

        /// <summary>
        /// Return the damage multiplier
        /// </summary>
        /// <returns>The damage multiplier</returns>
        public float GetDamageMultiplier()
        {
            return m_currentMultiplier;
        }

        /// <summary>
        /// If the ability is executed on start, this method will handle that.
        /// </summary>
        public void OnStart()
        {
            if (m_ability.GetWhen() == When.onStart)
            {
                ExecuteAbility();
            }

            m_onStopCalled = false;
        }

        /// <summary>
        /// If the ability is executed on stop, this method will handle that.
        /// </summary>
        public void OnStop()
        {
            //If the onstop ability has not been called yet, call it
            if (m_ability.GetWhen() == When.onStop && !m_onStopCalled)
            {
                ExecuteAbility();
                m_onStopCalled = true;
            }

            //Reset the values
            m_currentMultiplier = 1;
            m_firstColl = true;
            m_collider.isTrigger = false;
        }

        /// <summary>
        /// If the ability is executed on collision, this method will handle that.
        /// </summary>
        /// <param name="p_layer">The layer of the other collider.</param>
        /// <param name="p_isCatTurn">Whether or not this is the current cat turn.</param>
        public void OnCollision(int p_layer, bool p_isCatTurn)
        {
            if (!p_isCatTurn && m_ability.GetWhen() != When.onCatCollNonTurn || (p_isCatTurn && m_ability.GetWhen() == When.onCatCollNonTurn))
                return;
            
            
            //First, get the layer where are colliding with by name
            string l_layer = "";
            if (m_ability.GetWhen() == When.onCatColl || m_ability.GetWhen() == When.onCatCollNonTurn)
            {
                l_layer = "Cat";
            }
            else if (m_ability.GetWhen() == When.onWallColl)
            {
                l_layer = "Wall";
            }

            //If the layer is not one working with any abilities, return
            if (l_layer.Length == 0)
                return;
            
            //If we are colliding again and the ability should only work the first time
            if(m_ability.IsFirstCollOnly() && !m_firstColl)
                if (m_currentMultiplier > 1)
                    m_currentMultiplier = 1;
            
            //If the layer of the ability is not the same as the layer of the collider, return
            if (p_layer != LayerMask.NameToLayer(l_layer))
                return;
            
            //If the ability is a "First" and this is the first collision, trigger it
            if (m_ability.IsFirstCollOnly() || m_ability.IsAfterFirstColl())
            {
                if (!m_firstColl)
                {
                    return;
                }
                
                m_firstColl = false;
                ExecuteAbility();
            }
            //If the ability should always be triggered, trigger it
            else
            {
                ExecuteAbility();
            }
        }

        /// <summary>
        /// Execute the ability based on its type.
        /// </summary>
        private void ExecuteAbility()
        {
            switch (m_ability.GetType())
            {
                //If the ability is a multiplier, simply multiply the damages
                case Type.multiply_damage:
                    m_currentMultiplier += m_ability.GetValue();
                    break;
                case Type.throw_projectile:
                    DoThrow();
                    break;
                //If the ability is a ghost one, set the collider to trigger
                case Type.ghost:
                    m_collider.isTrigger = true;
                    break;
                case Type.zone_damage:
                    DoZoneDamage();
                    break;
            }
        }

        /// <summary>
        /// Execute zone damage abilities.
        /// </summary>
        private void DoZoneDamage()
        {
            //First call a OverlapCircleNonAlloc to get all the cats in the vicinity, using the cat transform, and the ability radius.
            Vector3 l_pos = m_transform.position;
            int l_nbColl = Physics2D.OverlapCircleNonAlloc(l_pos, m_ability.GetRadius(),m_circleCastColliders,1<<LayerMask.NameToLayer("Cat"));
            //If there is no cat, return
            if (l_nbColl == 0)
                return;
            
            //For each collision that is not this cat or null
            for (int i = 0; i < m_circleCastColliders.Length ; i++)
            {
                if(m_circleCastColliders[i] == null || m_circleCastColliders[i].transform == m_transform)
                    continue;

                //Get the cat component
                Cat l_cat = m_circleCastColliders[i].gameObject.GetComponent<Cat>();
                Health l_catHealth = m_circleCastColliders[i].gameObject.GetComponent<Health>();
                        
                //If the cat is not in our team, damage him
                if (l_cat.getTeam() != m_team && !l_cat.IsStunned())
                {
                    ushort l_points = l_catHealth.DealDamage(Mathf.FloorToInt(m_ability.GetValue()),DamageType.ability);
                    
                    if(l_points > 0)
                        m_gameController.IncreaseScore(m_team,l_points);
                }
            }
        }
        
        /// <summary>
        /// Execute throw abilities.
        /// </summary>
        private void DoThrow()
        {
            //First call a OverlapCircleNonAlloc to get all the cats in the vicinity, using the cat transform, and the ability radius.
            Vector3 l_pos = m_transform.position;
            int l_nbColl = Physics2D.OverlapCircleNonAlloc(l_pos, m_ability.GetRadius(),m_circleCastColliders,1<<LayerMask.NameToLayer("Cat"));
            //If there is no cat, return
            if (l_nbColl == 0)
                return;
            
            //If the ability should attack all the enemies
            if (m_ability.IsAttackAllEnemies())
            {
                
                //For each collision that is not this cat or null
                for (int i = 0; i < m_circleCastColliders.Length ; i++)
                {
                    if(m_circleCastColliders[i] == null || m_circleCastColliders[i].transform == m_transform)
                        continue;
                
                    Cat l_cat = m_circleCastColliders[i].gameObject.GetComponent<Cat>();
                    
                    //If the cat is not from this cat team
                    if (l_cat.getTeam() == m_team || l_cat.IsStunned())
                        continue;
                    
                    //Create a projectile and set it's target to the enemy cat
                    GameObject l_obj =
                        m_objectPooler.GetPooledObjectByName(m_ability.GetGameObject().name);
                    if (l_obj == null)
                    {
                        Debug.LogWarning("The pool did not return any pooled object");
                        return;
                    }
                    
                    Projectile l_proj = l_obj.GetComponent<Projectile>();
                    if (l_proj == null)
                    {
                        Debug.LogWarning("You cannot use a gameobject without the projectile script as projectile");
                        return;
                    }
                    
                    //Set lproj to active, and move him to transform
                    l_proj.gameObject.SetActive(true);
                    l_proj.transform.position = m_transform.position;
                    l_proj.Initialize(m_circleCastColliders[i].transform,m_gameController); 
                }
            }
            //If the ability should only attack the nearest enemy
            else
            {
                //Set the nearest distance to the max, just to be sure
                float l_nearestDist = float.MaxValue;
                Collider2D l_nearestCat = null;
                //For each collision that is not this cat or null
                for (int i = 0; i < m_circleCastColliders.Length ; i++)
                {
                    if(m_circleCastColliders[i] == null || m_circleCastColliders[i].transform == m_transform)
                        continue;

                    Cat l_cat = m_circleCastColliders[i].gameObject.GetComponent<Cat>();
                    
                    //If the cat is not from this cat team
                    if (l_cat.getTeam() == m_team || l_cat.IsStunned())
                        continue;
                
                    //Check the cat distance, and if it is closer to this cat than the current nearest one,
                    //Set it as the nearest one
                    float l_dist = (m_transform.position - m_circleCastColliders[i].transform.position).sqrMagnitude;
                    if (l_dist<l_nearestDist) {
                        l_nearestDist=l_dist;
                        l_nearestCat=m_circleCastColliders[i];
                    }
                }

                //If we got a cat in the given radius
                if (l_nearestCat)
                {
                    //Create a projectile and set it's target to the enemy cat
                    Projectile l_proj = m_objectPooler.GetPooledObjectByName(m_ability.GetGameObject().name)
                        .GetComponent<Projectile>();
                    if (l_proj == null)
                    {
                        Debug.LogWarning("You cannot use a gameobject without the projectile script as projectile");
                        return;
                    }
                    
                    //Set lproj to active, and move him to transform
                    l_proj.gameObject.SetActive(true);
                    l_proj.transform.position = m_transform.position;
                    l_proj.Initialize(l_nearestCat.transform,m_gameController); 
                }
            }
        }
    }
}
