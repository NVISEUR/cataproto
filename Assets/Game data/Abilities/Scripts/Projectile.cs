﻿using Game_data.Cats.Scripts;
using Game_data.Health_System.Scripts;
using Scenes.Game_scene.Script;
using UnityEngine;

namespace Game_data.Abilities.Scripts
{
	/// <summary>
	/// Handle a projectile life
	/// </summary>
	[RequireComponent(typeof(Rigidbody2D))]
	[DisallowMultipleComponent]
	public class Projectile : MonoBehaviour
	{
		/// <summary>
		/// The base damage of the projectile.
		/// </summary>
		[SerializeField] private int m_damage;
		/// <summary>
		/// The speed at which the projectile will travel.
		/// </summary>
		[SerializeField] private float m_force = 1f;
		/// <summary>
		/// The rigidbody of the projectile.
		/// <para>Used to throw the projectile toward the enemy.</para>
		/// </summary>
		[SerializeField] private Rigidbody2D m_rigidBody;
		/// <summary>
		/// The target team.
		/// <para>Used to increase the scores if needed.</para>
		/// </summary>
		private int m_targetTeam;
		/// <summary>
		/// The game controller.
		/// <para>Needed to increase the scores if needed.</para>
		/// </summary>
		private GameController m_gameController;

		private void Start()
		{
			if (m_rigidBody == null)
				m_rigidBody = GetComponent<Rigidbody2D>();
		}

		/// <summary>
		/// Throw the projectile toward the target.
		/// </summary>
		/// <param name="p_target">The target of the projectile.</param>
		/// <param name="p_gameController">The game controller.</param>
		public void Initialize(Transform p_target, GameController p_gameController)
		{
			//Get the target team
			m_targetTeam = p_target.gameObject.GetComponent<Cat>().getTeam();
			Vector2 l_dir = (p_target.position - transform.position).normalized;
			m_rigidBody.AddForce(l_dir * m_force);
			m_gameController = p_gameController;
		}

		/// <summary>
		/// On collision, deal damages if possible then destroy the projectile.
		/// </summary>
		/// <param name="other">The collider the projectile is colliding with.</param>
		private void OnTriggerEnter2D(Collider2D other)
		{
			Cat l_cat = other.gameObject.GetComponent<Cat>();
			//If the projectile hit a cat, that is of the opposite team and who is not stunned or a damageable object
			if ((other.gameObject.layer == LayerMask.NameToLayer("Cat") && l_cat.getTeam() == m_targetTeam && !l_cat.IsStunned())
				|| other.gameObject.layer == LayerMask.NameToLayer("Damageable"))
			{			
				//Deal the damages, and return the points if any
				ushort l_points = other.gameObject.GetComponent<Health>().DealDamage(m_damage,DamageType.ability);

				//Get the current team by reversing the target team
				int l_team = (m_targetTeam == 1) ? 2 : 1;
				
				//If the number of points is > 0, increase the current team score
				if(l_points > 0)
					m_gameController.IncreaseScore(l_team,l_points);
				
				//Deactivate the projectile to be reusable by the pool
				gameObject.SetActive(false);
			}
			//If the projectile collide with a wall, deactivate it
			else if (other.gameObject.layer == LayerMask.NameToLayer("Wall"))
			{
				gameObject.SetActive(false);
			}
		}
	}
}
