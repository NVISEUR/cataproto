using Game_data.Cats.Scripts;
using Ressources.Tools.StateMachine;
using Scenes.Game_scene.Script;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game_data.States
{
    /// <summary>
    /// Handle the behavior that should happen between the turns.
    /// </summary>
    public class BetweenTurnState : State
    {
        /// <summary>
        /// The turn ui text, to announce a new turn.
        /// </summary>
        [SerializeField] private TextMeshProUGUI m_turnText;
        /// <summary>
        /// The cat text.
        /// <para>Used to show the name and details of the cat who is going to play</para>
        /// </summary>
        [SerializeField] private TextMeshProUGUI m_catText;
        /// <summary>
        /// The background of the turn label.
        /// </summary>
        [SerializeField] private RawImage m_turnBackground;
        /// <summary>
        /// The scroll view containing the cats order.
        /// </summary>
        [SerializeField] private GameObject m_scrollView;
        /// <summary>
        /// The cat manager.
        /// <para>Used to get the next cat to play.</para>
        /// </summary>
        [SerializeField] private CatManager m_catManager;
        /// <summary>
        /// The game controller.
        /// <para>Used to check if the master is here.</para>
        /// </summary>
        [SerializeField] private GameController m_controller;
        /// <summary>
        /// Player one state.
        /// <para>Used to move to that state if needed.</para>
        /// </summary>
        [SerializeField] private State m_playerOneState;
        /// <summary>
        /// Player two state.
        /// <para>Used to move to that state if needed.</para>
        /// </summary>
        [SerializeField] private State m_playerTwoState;
        /// <summary>
        /// Master state.
        /// <para>Used to move to that state if needed.</para>
        /// </summary>
        [SerializeField] private State m_masterState;
        /// <summary>
        /// The time elapsed since the label s been shown.
        /// </summary>
        float m_elapsed = 0f;
        /// <summary>
        /// The time in second during witch the new turn label is shown.
        /// </summary>
        private float m_textShowingTime = 3;
        /// <summary>
        /// The color used by the player one's cats
        /// </summary>
        private static readonly Color m_p1Color = new Color(.2f, .3f, .8f);	
        /// <summary>
        /// The color used by the player two's cats
        /// </summary>
        private static readonly Color m_p2Color = new Color(.6f, .2f, .2f);
        /// <summary>
        /// The color used by the player two's cats
        /// </summary>
        private static readonly Color m_masterColor = new Color(.6f, .6f, .2f);
        /// <summary>
        /// The current player turn.
        /// <para>Either 1 or 2.</para>
        /// </summary>
        private int m_playerTurn;

        public override void OnStateEnter()
        {
            if (m_playerTurn == 2 && !m_controller.IsMasterHere())
            {
                m_playerTurn++;
                m_elapsed = m_textShowingTime;
            }
            else if (m_playerTurn == 3)
                m_playerTurn = 1;
            else
                m_playerTurn++;

            if (m_playerTurn == 1)
            {
                //Select the next cat
                m_catManager.SelectNextCat(m_playerTurn);
                //Set the turn texts and show it with the background
                m_turnText.text = "Blue turn";
                m_turnText.enabled = true;
                m_turnBackground.color = m_p1Color;
                m_turnBackground.enabled = true;
                m_scrollView.SetActive(true);
                CatData l_catData = m_catManager.GetPlayingCat().GetComponent<Cat>().getData();

                //Set the cat text and show it
                m_catText.text = l_catData.GetName() + "\n" + l_catData.GetAbility().GetDescription();
                m_catText.enabled = true;
            }
            else if (m_playerTurn == 2)
            {
                //Select the next cat
                m_catManager.SelectNextCat(m_playerTurn);
                //Set the turn texts and show it with the background
                m_turnText.text = "Red turn";
                m_turnText.enabled = true;
                m_turnBackground.color = m_p2Color;
                m_turnBackground.enabled = true;
                m_scrollView.SetActive(true);
			
                CatData l_catData = m_catManager.GetPlayingCat().GetComponent<Cat>().getData();

                //Set the cat text and show it
                m_catText.text = l_catData.GetName() + "\n" + l_catData.GetAbility().GetDescription();
                m_catText.enabled = true;
            }
            else if(m_controller.IsMasterHere())
            {
                //Set the turn texts and show it with the background
                m_turnText.text = "Master turn";
                m_turnText.enabled = true;
                m_turnBackground.color = m_masterColor;
                m_turnBackground.enabled = true;
                
                m_catText.text = "Beware of the master !";
                m_catText.enabled = true;
            }
        }

        public override State DoState()
        {
            if (m_elapsed <= m_textShowingTime)
            {
                //Add the delta time to the elapsed time
                m_elapsed += Time.deltaTime;
                //If we reached the threshold, hide the text and launch the new turn.
                if(m_elapsed >= m_textShowingTime)
                {
                    if (m_playerTurn == 1)
                        return m_playerOneState;
                    if (m_playerTurn == 2)
                        return m_playerTwoState;
                    
                    return m_masterState;
                }
            }

            return null;
        }

        public override void OnStateExit()
        {
            m_turnText.enabled = false;
            m_turnBackground.enabled = false;
            m_catText.enabled = false;
            m_scrollView.SetActive(false);
            
            m_elapsed = 0f;
        }
    }
}