﻿using Game_data.Abilities.Scripts;
using Game_data.Cats.Scripts;
using Ressources.Tools.StateMachine;
using Scenes.Game_scene.Script;
using UnityEngine;

namespace Game_data.States
{
	/// <summary>
	/// Handle the turn of a player
	/// </summary>
	public class PlayerTurnState : State
	{
		/// <summary>
		/// The circle gameobject around the joystick.
		/// </summary>
		[SerializeField] private GameObject m_touchLimit;
		/// <summary>
		/// The joystick gameobject.
		/// </summary>
		[SerializeField] private GameObject m_touchPad;
		/// <summary>
		/// The main camera, used to translate mouse input to world point.
		/// </summary>
		[SerializeField] private Camera m_camera;
		/// <summary>
		/// The next state.
		/// </summary>
		[SerializeField] private State m_nextState;
		/// <summary>
		/// The game controller.
		/// <para>Used to get some information.</para>
		/// </summary>
		[SerializeField] private GameController m_controller;
		/// <summary>
		/// The cat manager.
		/// <para>Used to get the current cat and check if any cat is moving.</para>
		/// </summary>
		[SerializeField] private CatManager m_catManager;
		/// <summary>
		/// The preview circle.
		/// <para>Used to show where the cat will collide when previewing.</para>
		/// </summary>
		[SerializeField] private GameObject m_previewCircle;
		/// <summary>
		/// The line renderer.
		/// <para>Used to draw the "preview" line ingame.</para>
		/// </summary>
		[SerializeField] private LineRenderer m_lineRenderer;
		/// <summary>
		/// Whether or not this turn has been played.
		/// </summary>
		private bool m_hasBeenPlayed;
		/// <summary>
		/// The number of frame without movement.
		/// </summary>
		private int m_frameWithoutMovement;
		/// <summary>
		/// The number of movement free frame before the player is considered stopped.
		/// </summary>
		private const int MOVEMENT_FREE_FRAME_BEFORE_END_OF_TURN = 3;
		/// <summary>
		/// The current cat preview layermask.
		/// </summary>
		private LayerMask m_catPreviewLayers;
		/// <summary>
		/// The transform of the selected cat.
		/// <para>Needed for previewing the throw.</para>
		/// </summary>
		private Transform m_selectedCatTransform;
		/// <summary>
		/// The currently selected cat radius
		/// <para>Needed for previewing the throw.</para>
		/// </summary>
		private float m_selectedCatRadius;
		/// <summary>
		/// The currently selected cat.
		/// </summary>
		private Cat m_selectedCat;
		/// <summary>
		/// Start position of the drag.
		/// <para>Used for previewing the throw.</para>
		/// </summary>
		private Vector3 m_startPos;
		/// <summary>
		/// End position of the drag
		/// <para>Used for previewing the throw.</para>
		/// </summary>
		private Vector3 m_endPos;
		/// <summary>
		/// The color to set the sprite renderer of the cat at when it is selected
		/// </summary>
		private static readonly Color SELECTED_COLOR = new Color(.15f, .6f, .2f);
		/// <summary>
		/// The last known mouse position.
		/// <para>To avoid redrawing preview lines if not needed.</para>
		/// </summary>
		private Vector3 m_lastMousePos;

		//Change the current cat, its color, ...
		public override void OnStateEnter()
		{
			//Change the two selected-members with the right cat
			ChangeSelectedCat(m_catManager.GetPlayingCat());
			
			//Initialise the number of points for the line renderer to one
			m_lineRenderer.positionCount = 1;
		}

		public override State DoState()
		{
			//Check if the turn can end
			if (m_hasBeenPlayed && !m_catManager.IsAnyCatMoving())
			{
				m_frameWithoutMovement++;
				if (m_frameWithoutMovement >= MOVEMENT_FREE_FRAME_BEFORE_END_OF_TURN)
				{
					return m_nextState;
				}
			}
			//Handle inputs.
			else if (!m_hasBeenPlayed)
			{
				if (Input.GetMouseButtonDown(0))
				{
					//Initialize the preview circle, and get the starting position
					Vector3 l_catScale = m_selectedCatTransform.localScale;
					m_previewCircle.SetActive(true);
					m_previewCircle.transform.localScale = l_catScale;
					m_startPos = m_camera.ScreenToWorldPoint(Input.mousePosition);
					
					//Get the cat position
					Vector3 l_catPos = m_selectedCatTransform.position;
					//Draw the line starting from the cat
					m_lineRenderer.startWidth = m_selectedCatRadius*l_catScale.x;
					m_lineRenderer.endWidth = m_selectedCatRadius*l_catScale.x;
					m_lineRenderer.SetPosition(0,l_catPos);
					
					//Activate the joystick and set it's position to the start pos
					m_touchLimit.SetActive(true);
					m_touchLimit.transform.position = new Vector3(m_startPos.x,m_startPos.y,m_touchLimit.transform.position.z);
					m_touchPad.SetActive(true);
					m_touchPad.transform.position = new Vector3(m_startPos.x,m_startPos.y,m_touchPad.transform.position.z);
				}
				
				if (Input.GetMouseButton(0) && Input.mousePosition != m_lastMousePos)
				{
					//Initialize the line renderer positions to 1
					m_lineRenderer.positionCount = 1;
					//Transform mouse position from screen space to world space
					m_endPos = m_camera.ScreenToWorldPoint(Input.mousePosition);
					
					//The position of the current point
					Vector2 l_pos = m_selectedCatTransform.position;
					//The direction in which the cat will move
					Vector2 l_dir = m_startPos - m_endPos;
					//The clamped magnitude used to limit the joystick movement inside it's circle
					Vector2 l_clampedMagnitude = Vector2.ClampMagnitude(l_dir, 1f) *-1f;
					//Move the joystick according to the current position of the mouse
					m_touchPad.transform.position = new Vector3(m_startPos.x+l_clampedMagnitude.x,m_startPos.y+l_clampedMagnitude.y,m_touchPad.transform.position.z);
					//If the clampedmagnitude magnitude is below 0.5, it means that we are cancelling the throw
					if (l_clampedMagnitude.magnitude < 0.5)
						return null;
					
					//The index of the current preview line
					int l_lineIndex;
					
					//Circle cast to imitate the cat throw, then store the eventual hit
					RaycastHit2D hit = Physics2D.CircleCast(l_pos, m_selectedCatRadius*m_selectedCatTransform.localScale.x, l_dir, float.MaxValue,m_catPreviewLayers);

					//If there is a hit
					if (hit)
					{
						//Get the position and direction of the hit
						l_pos = hit.centroid;
						l_dir = Vector2.Reflect(l_dir, hit.normal).normalized;
						//Move the preview circle to the centroid of the circlecast
						m_previewCircle.transform.position = l_pos;
						//Increment the number of position of the line renderer
						l_lineIndex = m_lineRenderer.positionCount++;
						//Set the new position of the line renderer to the centroid hit
						m_lineRenderer.SetPosition(l_lineIndex,l_pos);
						//Increment the number of position of the line renderer
						l_lineIndex = m_lineRenderer.positionCount++;
						//Set the direction preview position
						m_lineRenderer.SetPosition(l_lineIndex,l_pos+l_dir);
					}
					//Store the mouse position to avoid redrawing lines if not needed
					m_lastMousePos = Input.mousePosition;
				}
				
				//Throw the selected cat when releasing the mouse, and reset the throw-related variables
				if (Input.GetMouseButtonUp(0))
				{
					//Get the direction opposing the movement (start-end)
					Vector3 l_dir = m_startPos - m_endPos;
					//Get the clamped magnitude of the joystick
					Vector2 l_clampedMagnitude = Vector2.ClampMagnitude(l_dir, 1f) *-1f;
					//If the clampedmagnitude magnitude is below 0.5, it means that we are cancelling the throw
					if (l_clampedMagnitude.magnitude < 0.5)
					{
						ResetThrow();
						return null;
					}
					//Throw the cat based on the direction
					m_selectedCat.Throw(l_dir.normalized*3);
					ResetThrow();
					m_hasBeenPlayed = true;
				}
			}

			return null;
		}

		//Reset everything, ...
		public override void OnStateExit()
		{
			m_hasBeenPlayed = false;
			m_frameWithoutMovement = 0;
			ResetThrow();
			//First check if the cat exists
			if (m_selectedCat == null)
				return;
			
			//Then revert his color to the original one
			m_selectedCat.ResetColor();
			
			//Then put both the components to null
			m_selectedCat = null;
			m_selectedCatTransform = null;
		}
		
		
		/// <summary>
		/// Reset the thrown-related members
		/// </summary>
		private void ResetThrow()
		{
			//Reset both the starting and ending point of the line to zero
			m_startPos = Vector3.zero;
			m_endPos = Vector3.zero;
			//Reset the line renderer with the above values
			m_lineRenderer.positionCount = 1;

			m_touchLimit.SetActive(false);
			m_touchPad.SetActive(false);
			m_previewCircle.SetActive(false);
		}
		
		/// <summary>
		/// Change the selected cat relative members based on the new turn cat.
		/// </summary>
		/// <param name="p_cat">The cat that is going to play this turn.</param>
		private void ChangeSelectedCat(GameObject p_cat)
		{
			//Get the Cat and Transform component of the cat
			m_selectedCat = p_cat.GetComponent<Cat>();
			m_selectedCatTransform = p_cat.transform;
			//Change the current cat color
			m_selectedCat.ChangeColor(SELECTED_COLOR);
			//Execute the onStart cat ability if any
			m_selectedCat.OnStart();

			//Get the radius of the selected cat collider, and change the preview layermask to fit the current cat
			m_selectedCatRadius = m_selectedCat.GetComponent<CircleCollider2D>().radius;
			m_catPreviewLayers = m_controller.GetPreviewLayers(m_selectedCat.getData().GetAbility().GetType() == Type.ghost);
		}
	}
}
