using Ressources.Tools.StateMachine;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Game_data.States
{
    /// <summary>
    /// Handle the tutorials.
    /// </summary>
    public class TutorialState : State
    {
        /// <summary>
        /// The tutorial object.
        /// <para>Used to display the tutorial.</para>
        /// </summary>
        [SerializeField] private GameObject m_tutorialObject;
        /// <summary>
        /// The turn ui text, to announce a new turn.
        /// </summary>
        [SerializeField] private TextMeshProUGUI m_turnText;
        /// <summary>
        /// The cat text.
        /// <para>Used to show the name and details of the cat who is going to play</para>
        /// </summary>
        [SerializeField] private TextMeshProUGUI m_catText;
        /// <summary>
        /// The background of the turn label.
        /// </summary>
        [SerializeField] private RawImage m_turnBackground;
        /// <summary>
        /// The scrollview of the turn label.
        /// </summary>
        [SerializeField] private GameObject m_scrollView;
        /// <summary>
        /// The next state.
        /// </summary>
        [SerializeField] private State m_nextState;

        public override void OnStateEnter()
        {
            m_turnText.enabled = false;
            m_turnBackground.enabled = false;
            m_catText.enabled = false;
            m_scrollView.SetActive(false);
            m_tutorialObject.SetActive(true);
        }

        public override State DoState()
        {
            if (!m_tutorialObject.activeSelf)
            {
                return m_nextState;
            }

            return null;
        }

        public override void OnStateExit()
        {
        }
    }
}