using Ressources.Tools.StateMachine;
using Scenes.Game_scene.Script;
using UnityEngine;

namespace Game_data.States
{
    /// <summary>
    /// Handle the behavior that should happen during the master's turn.
    /// </summary>
    public class MasterTurnState : State
    {
        /// <summary>
        /// The threshold before the master storm into the room.
        /// </summary>
        [SerializeField] private int m_rageThreshold;
        /// <summary>
        /// The positions where the master can spawn.
        /// </summary>
        [SerializeField] private Transform[] m_masterSpawnPoints;
        /// <summary>
        /// The prefab of the master.
        /// </summary>
        [SerializeField]  private GameObject m_masterPrefab;
        /// <summary>
        /// The next state.
        /// </summary>
        [SerializeField] private State m_nextState;
        /// <summary>
        /// The game controller.
        /// <para>Used to notify that the master is here, and get some information</para>
        /// </summary>
        [SerializeField] private GameController m_controller;
        /// <summary>
        /// The warning display.
        /// <para>Used to notify the players that the master is coming.</para>
        /// </summary>
        [SerializeField] private GameObject m_masterWarning;
        /// <summary>
        /// Random.
        /// <para>Used to randomly select one of the possible spawn points for the master.</para>
        /// </summary>
        private System.Random m_random = new System.Random();
        /// <summary>
        /// Whether or not the master is there.
        /// </summary>
        private bool m_masterIsHere;
        /// <summary>
        /// The master controller, to launch the master.
        /// </summary>
        private MasterController m_master;
        /// <summary>
        /// The number of turn since the last time the master attacked.
        /// <para>Set to -2 to ignore the turn when the master is comming.</para>
        /// </summary>
        private int m_turnSinceMasterAttacked = 0;
        /// <summary>
        /// Whether or not the master has been sent.
        /// </summary>
        private bool m_masterSent;
        /// <summary>
        /// The number of movement free frame before the master is considered stopped.
        /// </summary>
        private const int MOVEMENT_FREE_FRAME_BEFORE_END_OF_TURN = 3;
        /// <summary>
        /// The number of frame without movement.
        /// </summary>
        private int m_frameWithoutMovement;
        /// <summary>
        /// The time elapsed since the warning has been shown.
        /// </summary>
        private float m_elapsed;
        /// <summary>
        /// The amount of time the warning should be shown.
        /// </summary>
        private float m_warningShowingTime = 3;

        public override void OnStateEnter()
        {
            if(m_masterIsHere)
                m_turnSinceMasterAttacked++;
			
            m_controller.NewTurn();
        }

        public override State DoState()
        {
            if (m_masterIsHere)
            {
                if (m_masterSent && !m_master.IsMoving())
                {				
                    m_frameWithoutMovement++;
                    if (m_frameWithoutMovement >= MOVEMENT_FREE_FRAME_BEFORE_END_OF_TURN)
                    {
                        return m_nextState;
                    }
                }
                
                if (m_turnSinceMasterAttacked >= m_controller.GetTurnBetweenMasterAttacks())
                {
                    m_turnSinceMasterAttacked = 0;
                    //Send the master attack the cats
                    m_master.SendMaster();

                    m_masterSent = true;
                }
                else if(!m_masterSent)
                {
                    return m_nextState;
                }
            }
            else
            {
                if (m_controller.GetPlayerOneScore() + m_controller.GetPlayerTwoScore() >= m_rageThreshold)
                {
                    //Show warning
                    m_masterWarning.SetActive(true);
                    if (m_elapsed <= m_warningShowingTime)
                    {
                        //Add the delta time to the elapsed time
                        m_elapsed += Time.deltaTime;
                        //If we reached the threshold, hide the warning and launch the new turn.
                        if(m_elapsed >= m_warningShowingTime)
                        {
                            m_masterWarning.SetActive(false);
                            SpawnMaster();
                    
                            return null;
                        }
                    }
                }
                else
                {
                    return m_nextState;
                }
            }

            return null;
        }

        /// <summary>
        /// Instantiate the master object and notify the game manager.
        /// </summary>
        private void SpawnMaster()
        {
            m_masterIsHere = true;
            m_controller.StartCountdown();
            Transform l_spawnPoint = m_masterSpawnPoints[m_random.Next(m_masterSpawnPoints.Length)];
            m_master = Instantiate(m_masterPrefab, l_spawnPoint.position, Quaternion.identity).GetComponent<MasterController>();
        }

        public override void OnStateExit()
        {
            m_masterSent = false;
        }
    }
}