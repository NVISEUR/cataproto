﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Game_data.Tutorials.Scripts
{
    /// <summary>
    /// Handle the tutorial displaying.
    /// </summary>
    [DisallowMultipleComponent]
    public class TutorialController : MonoBehaviour, IPointerClickHandler
    {
        /// <summary>
        /// The animator used by the tutorial.
        /// </summary>
        [SerializeField] private Animator m_animator;
        /// <summary>
        /// The tutorial text changed by the animator via events.
        /// </summary>
        [SerializeField] private TextMeshProUGUI m_text;
        /// <summary>
        /// The hash representation of the first animator trigger.
        /// </summary>
        private static readonly int m_secondSectionHash = Animator.StringToHash("SecondPart");
        /// <summary>
        /// The hash representation of the second animator trigger.
        /// </summary>
        private static readonly int m_thirdSectionHash = Animator.StringToHash("ThirdPart");
        /// <summary>
        /// The hash representation of the third animator trigger.
        /// </summary>
        private static readonly int m_fourthSectionHash = Animator.StringToHash("FourthPart");
        /// <summary>
        /// The current tutorial section.
        /// </summary>
        private int m_section = 1;

        public void OnPointerClick(PointerEventData eventData)
        {
            if (m_section == 1)
            {
                m_animator.SetBool(m_secondSectionHash,true);
                m_section++;
            }
            else if (m_section == 2)
            {
                m_animator.SetBool(m_thirdSectionHash,true);
                m_section++;
            }
            else if (m_section == 3)
            {
                m_animator.SetBool(m_fourthSectionHash,true);
                m_section++;
            }
            else
            {
                gameObject.SetActive(false);
            }
        }

        /// <summary>
        /// Change the text on screen.
        /// <para>Called by the animator events.</para>
        /// </summary>
        /// <param name="text">The text we wish to show on screen.</param>
        public void ChangeText(string text)
        {
            m_text.text = text;
        }
    }
}
