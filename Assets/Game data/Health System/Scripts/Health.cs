﻿using Game_data.Cats.Scripts;
using Game_data.Environmental_effects.Scripts;
using Game_data.Floating_damages.Scripts;
using Ressources.Tools.ObjectPooling;
using UnityEngine;

namespace Game_data.Health_System.Scripts
{
	/// <summary>
	/// The type of damages.
	/// <para>Used to differentiate the sources from which the damages are coming.</para>
	/// </summary>
	public enum DamageType
	{
		master,
		environmental,
		ability,
		normal
	}
	
	/// <summary>
	/// Handle all the health-related functionalities.
	/// </summary>
	[DisallowMultipleComponent]
	public class Health : MonoBehaviour
	{
		/// <summary>
		/// The score given to the player when the health reach 0.
		/// </summary>
		[SerializeField] private ushort m_scoreOnDestruction;
		/// <summary>
		/// Whether or not the object can be healed by negative attack
		/// </summary>
		[SerializeField] private bool m_healByAttack;
		/// <summary>
		/// The health bar component.
		/// <para>The health bar is optional.</para>
		/// </summary>
		[SerializeField] private HealthBar m_healthBar;
		/// <summary>
		/// The max health of the gameobject.
		/// </summary>
		[SerializeField] private float m_maxHealth = 100;
		/// <summary>
		/// The floating damage controller.
		/// <para>Used to show floating damages.</para>
		/// </summary>
		[SerializeField] private FloatingDamagesController m_fdController;
		/// <summary>
		/// The gameobject which should be instantiated on death.
		/// <para>This is optional.</para>
		/// </summary>
		[SerializeField] private EffectData m_onDeathEffect;
		/// <summary>
		/// The effect object name.
		/// </summary>
		[SerializeField] private string m_effectObjectName;
		/// <summary>
		/// The object pooler to get the effect object.
		/// </summary>
		[SerializeField] private ObjectPooler m_pooler;
		/// <summary>
		/// The current health of the gameobject.
		/// </summary>
		private float m_currentHealth;
		/// <summary>
		/// The transform of the gameobject.
		/// <para>Used when calling to show the damages.</para>
		/// </summary>
		private Transform m_transform;

		private void Start()
		{
			if(m_healthBar == null)
				m_healthBar = GetComponentInChildren<HealthBar>();
			if (m_fdController == null)
				m_fdController = FindObjectOfType<FloatingDamagesController>();
		
			//Initialise the current health to it's maximum
			m_currentHealth = m_maxHealth;
			m_transform = transform;
		}

		/// <summary>
		/// Change the max health of the gameobject.
		/// </summary>
		/// <param name="p_maxHealth">The new amount of max health.</param>
		public void SetMaxHealth(int p_maxHealth)
		{
			m_maxHealth = p_maxHealth;
			m_currentHealth = p_maxHealth;
		}

		/// <summary>
		/// Heal the object by the given percentage.
		/// </summary>
		/// <param name="p_percentage">The percentage that should be healed (e.g. : 50)</param>
		/// <returns>Whether or not the gameobject has been fully healed.</returns>
		public bool Heal(float p_percentage)
		{
			m_currentHealth += m_maxHealth * (p_percentage/100);
			
			if (m_currentHealth >= m_maxHealth)
			{
				m_currentHealth = m_maxHealth;
				if(m_healthBar != null)
					m_healthBar.UpdateHealthPct(m_currentHealth/m_maxHealth);
				return true;
			}
			if(m_healthBar != null)
				m_healthBar.UpdateHealthPct(m_currentHealth/m_maxHealth);

			return false;
		}

		/// <summary>
		/// Deal damages to the gameobject.
		/// </summary>
		/// <param name="p_damage">The amount of damages do deal.</param>
		/// <param name="p_damageType">The type of the damages.</param>
		/// <returns>The amount of score granted by the attack.</returns>
		public ushort DealDamage(int p_damage, DamageType p_damageType)
		{
			if (p_damage < 0 && !m_healByAttack)
				return 0;
			
			m_currentHealth -= p_damage;

			//Show the damages on screen
			m_fdController.ShowDamage(m_transform.position, p_damage,p_damageType);

			if (m_currentHealth > m_maxHealth)
				m_currentHealth = m_maxHealth;
			
			if (m_currentHealth <= 0)
			{
				m_currentHealth = 0;
				
				Cat l_cat = GetComponent<Cat>();
				//If the cat's health is at 0, stun it
				if (l_cat)
				{
					l_cat.Stun();
				}
				//Else, deactivate the gameobject and check if there is any ondeath effect.
				else
				{
					gameObject.SetActive(false);

					//If there is a ondeath effect, instantiate it
					if (m_onDeathEffect != null)
					{
						EnvironmentalEffect l_effect = m_pooler.GetPooledObjectByName(m_effectObjectName).GetComponent<EnvironmentalEffect>();
						l_effect.SetData(m_onDeathEffect);

						l_effect.transform.position = m_transform.position;
						l_effect.gameObject.SetActive(true);
					}
				}
		
				//The health bar is optional
				if(m_healthBar != null)
					m_healthBar.UpdateHealthPct(m_currentHealth/m_maxHealth);
				
				return m_scoreOnDestruction;
			}
		
			//The health bar is optional
			if(m_healthBar != null)
				m_healthBar.UpdateHealthPct(m_currentHealth/m_maxHealth);

			return 0;
		}
	
	}
}
