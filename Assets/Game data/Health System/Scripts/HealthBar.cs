﻿using UnityEngine;
using UnityEngine.UI;

namespace Game_data.Health_System.Scripts
{
	/// <summary>
	/// Handle the visual health bar in game.
	/// </summary>
	[DisallowMultipleComponent]
	[RequireComponent(typeof(RectTransform))]
	[RequireComponent(typeof(Image))]
	public class HealthBar : MonoBehaviour
	{
		/// <summary>
		/// The transform of the health bar.
		/// </summary>
		[SerializeField] private RectTransform m_healthImage;
		/// <summary>
		/// The image of the health bar.
		/// <para>Used to change it's color based on the %.</para>
		/// </summary>
		[SerializeField] private Image m_image;
		/// <summary>
		/// The color of the health bar when it is full.
		/// </summary>
		[SerializeField] private Color m_maxHealthColor;
		/// <summary>
		/// The color of the health bar when it is empty.
		/// </summary>
		[SerializeField] private Color m_minHealthColor;
		/// <summary>
		/// The percentage of health.
		/// <para>Used to set the health bar position, size and color.</para>
		/// </summary>
		private float m_healthPct;
		/// <summary>
		/// The maximum x position of the bar.
		/// </summary>
		private const float MAX_X_POS = -.38f;
		/// <summary>
		/// The maximum width of the bar
		/// </summary>
		private float m_maxWidth;

		private void Start()
		{
			if (m_healthImage == null)
				m_healthImage = GetComponent<RectTransform>();
			if (m_image == null)
				m_image = GetComponent<Image>();

			m_maxWidth = m_healthImage.rect.width;

			m_image.color = m_maxHealthColor;
		}

		/// <summary>
		/// Update the width and the position of the health bar according to the given health %.
		/// </summary>
		/// <param name="p_health">The current health % of the gameobject.</param>
		public void UpdateHealthPct(float p_health)
		{
			m_healthPct = p_health;

			//Change the color of the health bar based on the health pct using lerp
			m_image.color = Color.Lerp(m_minHealthColor, m_maxHealthColor,m_healthPct);
			//Change the size of the health bar based on the remaining pct
			m_healthImage.sizeDelta = new Vector2(m_maxWidth * m_healthPct,m_healthImage.sizeDelta.y);
			//Change the position of the health bar based on the remaining pct
			Vector3 l_position = m_healthImage.localPosition;
			m_healthImage.localPosition = new Vector3(MAX_X_POS * (1-m_healthPct), l_position.y, l_position.z);
		}
	}
}
