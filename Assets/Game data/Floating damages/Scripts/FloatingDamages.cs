﻿using UnityEngine;

namespace Game_data.Floating_damages.Scripts
{
	/// <summary>
	/// Handle the behaviour of a floating damage object.
	/// </summary>
	[DisallowMultipleComponent]
	public class FloatingDamages : MonoBehaviour
	{
		/// <summary>
		/// Set the floating damage to inactive.
		/// <para>Called by an animator event.</para>
		/// </summary>
		public void SetInactive()
		{
			gameObject.SetActive(false);
		}
	}
}
	