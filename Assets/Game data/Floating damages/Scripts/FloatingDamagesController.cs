﻿using System;
using Game_data.Health_System.Scripts;
using Ressources.Tools.ObjectPooling;
using TMPro;
using UnityEngine;

namespace Game_data.Floating_damages.Scripts
{
	/// <summary>
	/// Handle the floating damages on the scene.
	/// </summary>
	[DisallowMultipleComponent]
	public class FloatingDamagesController : MonoBehaviour
	{
		/// <summary>
		/// The object pooler.
		/// <para>Used to get a floating damages object when needed.</para>
		/// </summary>
		[SerializeField] private ObjectPooler m_objectPooler;
		/// <summary>
		/// The current floating damage object.
		/// </summary>
		[SerializeField] private GameObject m_damageObject;
		/// <summary>
		/// The text color when it is a normal damage.
		/// </summary>
		[SerializeField] private Color m_normalDamageColor;
		/// <summary>
		/// The text color when it is a damage dealt by the master.
		/// </summary>
		[SerializeField] private Color m_masterDamageColor;
		/// <summary>
		/// The text color when it is a ability damage.
		/// </summary>
		[SerializeField] private Color m_abilityDamageColor;
		/// <summary>
		/// The text color when it is an environmental damage.
		/// </summary>
		[SerializeField] private Color m_environmentalDamageColor;

		/// <summary>
		/// Show the damage dealt on the screen.
		/// </summary>
		/// <param name="p_position">The position of the gameobject that took damages.</param>
		/// <param name="p_value">The amount of damages done.</param>
		/// <param name="p_type">The type of damages done.</param>
		/// <exception cref="ArgumentOutOfRangeException"></exception>
		public void ShowDamage(Vector3 p_position,int p_value, DamageType p_type)
		{
			//Get the damage from the pool
			GameObject l_damage = m_objectPooler.GetPooledObjectByName(m_damageObject.name);

			if (l_damage == null)
				return;
		
			//Get the screen position (Because the floating damage object is on the canvas)
			Vector3 l_pos = RectTransformUtility.WorldToScreenPoint(Camera.main,p_position);
			//Set the position of the floating damage object
			l_damage.transform.position = l_pos;
			//Activate the floating damage object
			l_damage.SetActive(true);
			//Get the text of the floating damage object 
			TextMeshProUGUI l_text = l_damage.GetComponentInChildren<TextMeshProUGUI>();
			//Set it to the damage value
			l_text.text = p_value.ToString();
			//Set the text color based on the type of damage
			switch (p_type)
			{
				case DamageType.master:
					l_text.color = m_masterDamageColor;
					break;
				case DamageType.environmental:
					l_text.color = m_environmentalDamageColor;
					break;
				case DamageType.ability:
					l_text.color = m_abilityDamageColor;
					break;
				case DamageType.normal:
					l_text.color = m_normalDamageColor;
					break;
				default:
					throw new ArgumentOutOfRangeException(nameof(p_type), p_type, null);
			}
		}
	}
}
