using UnityEngine;

namespace Game_data.Items.Scripts
{
    /// <summary>
    /// Scriptable object representing a list of items.
    /// </summary>
    [CreateAssetMenu(fileName = "ItemList", menuName = "Data/ItemList", order = 2)]
    public class ItemList : ScriptableObject
    {
        /// <summary>
        /// The list of items.
        /// </summary>
        [SerializeField] private Item[] m_cloaks;
        [SerializeField] private Item[] m_hats;
        [SerializeField] private Item[] m_collars;

        public Item[] GetCloaks()
        {
            return m_cloaks;
        }
        
        public Item[] GetHats()
        {
            return m_hats;
        }
        
        public Item[] GetCollars()
        {
            return m_collars;
        }
    }
}