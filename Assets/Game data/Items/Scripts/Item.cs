using System;
using UnityEngine;

namespace Game_data.Items.Scripts
{
    /// <summary>
    /// The possible types of items.
    /// </summary>
    public enum ItemType
    {
        collar,
        cloak,
        hat
    }

    public enum ItemEffectType
    {
        ChangeDamage,
        ChangeHealth,
        ChangeSize,
        ChangeForce
    }

    [Serializable]
    public class ItemEffect
    {
        [SerializeField] private ItemEffectType m_type;
        [SerializeField] private float m_value;

        public new ItemEffectType GetType()
        {
            return m_type;
        }    

        public float GetValue()
        {
            return m_value;
        }
    }
	
    /// <summary>
    /// Scriptable object representing the data of a particular item.
    /// </summary>
    [Serializable]
    public class Item
    {
        /// <summary>
        /// The name of the object
        /// </summary>
        [SerializeField] private string m_name;
        /// <summary>
        /// The type of the item.
        /// </summary>
        [SerializeField] private ItemType m_type;
        /// <summary>
        /// Icon of the item
        /// </summary>
        [SerializeField] private Texture m_icon;
        /// <summary>
        /// The effects of the item.
        /// </summary>
        [SerializeField] private ItemEffect m_firstEffect;
        [SerializeField] private ItemEffect m_secondEffect;

        [SerializeField] private string m_description;

        public string GetDescription()
        {
            return m_description;
        }
        
        public string GetName()
        {
            return m_name;
        }
        
        public new ItemType GetType()
        {
            return m_type;
        }

        public Texture GetIcon()
        {
            return m_icon;
        }

        public ItemEffect GetFirstEffect()
        {
            return m_firstEffect;
        }
        
        public ItemEffect GetSecondEffect()
        {
            return m_secondEffect;
        }
    }
}