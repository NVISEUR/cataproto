﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Game_data.Items.Scripts
{
    public class ItemDetails : MonoBehaviour, IPointerClickHandler
    {
        [SerializeField] private RawImage m_itemImage;
        [SerializeField] private TextMeshProUGUI m_itemName;
        [SerializeField] private TextMeshProUGUI m_itemEffectOne;
        [SerializeField] private TextMeshProUGUI m_itemEffectTwo;
        [SerializeField] private TextMeshProUGUI m_itemDescription;

        public void Initialize(Texture p_image, string p_name, string p_effectOne, string p_effectTwo, string p_description)
        {
            m_itemImage.texture = p_image;
            m_itemName.text = p_name;
            m_itemEffectOne.text = p_effectOne;
            m_itemEffectTwo.text = p_effectTwo;
            m_itemDescription.text = p_description;
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            gameObject.SetActive(false);
        }
    }
}
