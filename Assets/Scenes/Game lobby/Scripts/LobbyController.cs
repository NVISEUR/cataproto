﻿using Scenes.General.Scripts;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Scenes.Game_lobby.Scripts
{
	public class LobbyController : MonoBehaviour
	{
		[SerializeField] private DataManager m_manager;
		
		public void LaunchMatch()
		{
			m_manager.SelectCat(4);
			m_manager.SelectCat(1);
			m_manager.SelectCat(2);
			m_manager.SelectCat(3);
			
			SceneManager.LoadScene("GameScene");
		}

		private void Update()
		{
			if (Input.GetKeyDown(KeyCode.Escape))
			{
				SceneManager.LoadScene("MainMenu");
			}
		}
	}
}
