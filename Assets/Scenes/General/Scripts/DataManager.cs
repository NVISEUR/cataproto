﻿using System.Linq;
using UnityEngine;

namespace Scenes.General.Scripts
{
    /// <summary>
    /// Handle the data between the scene and the players.
    /// </summary>
    [DisallowMultipleComponent]
    public class DataManager : MonoBehaviour
    {
        /// <summary>
        /// The maximum of cats by team the players can have.
        /// </summary>
        private const int MAX_CATS_BY_TEAM = 4;
        /// <summary>
        /// The bytes representing the selected cats.
        /// </summary>
        private static readonly byte[] m_selectedCat = new byte[MAX_CATS_BY_TEAM];
        /// <summary>
        /// The score of the first player.
        /// </summary>
        private static ushort m_playerOneScore;
        /// <summary>
        /// the score of the second player.
        /// </summary>
        private static ushort m_playerTwoScore;
        /// <summary>
        /// The instance of the data manager.
        /// <para>Used to avoid instantiating more that one data manager.</para>
        /// </summary>
        private static DataManager m_instance;
    
        //Do not destroy the gameobject when changing scene, to access the selected cats
        private void Awake()
        {
            if (m_instance == null)
            {
                m_instance = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        #region CatData
        
        /// <summary>
        /// Add the given cat id to the selected cat array if possible.
        /// </summary>
        /// <param name="p_id">The id of the cat we wish to select.</param>
        /// <returns>True if the cat has been added.</returns>
        public bool SelectCat(byte p_id)
        {
            //If the id is already in the array, return
            if (m_selectedCat.Any(id => id == p_id))
                return false;
            
            //Add the id to the array if possible
            for(int i = 0; i < MAX_CATS_BY_TEAM; i++)
            {
                if (m_selectedCat[i] == 0)
                {
                    m_selectedCat[i] = p_id;
                    return true;
                }
            }

            return false;
        }
    
        /// <summary>
        /// Remove the given id from the selected cat array if possible.
        /// </summary>
        /// <param name="p_id">The id of the cat we wish to remove.</param>
        /// <returns>True if the cat has been unselected.</returns>
        public bool UnselectCat(byte p_id)
        {
            //If the id is not in the array, return
            if (m_selectedCat.All(id => id != p_id))
                return true;
            
            for(int i = 0; i < MAX_CATS_BY_TEAM; i++)
            {
                if (m_selectedCat[i] == p_id)
                {
                    m_selectedCat[i] = 0;
                    break;
                }
            }

            return true;
        }

        public void UnselectAllCat()
        {
            for(int i = 0; i < MAX_CATS_BY_TEAM; i++)
            {
                m_selectedCat[i] = 0;
            }
        }
    
        /// <summary>
        /// Return the selected cat array
        /// </summary>
        /// <returns>The selected cat array</returns>
        public byte[] GetSelectedCats()
        {
            return m_selectedCat;
        }

        public bool IsSelectd(byte p_id)
        {
            for(int i = 0; i < MAX_CATS_BY_TEAM; i++)
            {
                if (m_selectedCat[i] == p_id)
                {
                    return true;
                }
            }

            return false;
        }
        #endregion

        #region Score

        public void SetPlayerOneScore(ushort p_playerOneScore)
        {
            m_playerOneScore = p_playerOneScore;
        }
        
        public ushort GetPlayerOneScore()
        {
            return m_playerOneScore;
        }
        
        public void SetPlayerTwoScore(ushort p_playerTwoScore)
        {
            m_playerTwoScore = p_playerTwoScore;
        }
        
        public ushort GetPlayerTwoScore()
        {
            return m_playerTwoScore;
        }

        #endregion
    }
}
