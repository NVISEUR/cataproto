﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Scenes.General.Scripts
{
	/// <summary>
	/// Handle switching between scene.
	/// </summary>
	[DisallowMultipleComponent]
	public class SceneSwitcher : MonoBehaviour 
	{
		/// <summary>
		/// Change the scene by the one corresponding to the string parameter.
		/// </summary>
		/// <param name="p_sceneName">The name of the scene we want to reach.</param>
		public void SwitchScene(string p_sceneName)
		{
			SceneManager.LoadScene(p_sceneName);
		}

		/// <summary>
		/// Quit the game.
		/// </summary>
		public void LeaveGame()
		{
			Application.Quit();
		}
	}
}
