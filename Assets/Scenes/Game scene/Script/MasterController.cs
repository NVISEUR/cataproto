﻿using Game_data.Cats.Scripts;
using Game_data.Health_System.Scripts;
using TMPro;
using UnityEngine;

namespace Scenes.Game_scene.Script
{
    /// <summary>
    /// Handle the behavior of the master.
    /// </summary>
    [DisallowMultipleComponent]
    [RequireComponent(typeof(Rigidbody2D))]
    public class MasterController : MonoBehaviour
    {
        /// <summary>
        /// The force to launch the master with.
        /// </summary>
        [SerializeField] private float m_force = 500;
        /// <summary>
        /// The radius in which the master will look for cats.
        /// </summary>
        [SerializeField] private int m_radius = 25;
        /// <summary>
        /// The damage the master will do to the cats.
        /// </summary>
        [SerializeField] private int m_damage = 50;
        /// <summary>
        /// The rigidbody of the master.
        /// <para>Used to launch him toward the nearest cat.</para>
        /// </summary>
        [SerializeField] private Rigidbody2D m_rigidBody;
        /// <summary>
        /// The remaining turn text.
        /// <para>To show the number of remaining turns.</para>
        /// </summary>
        [SerializeField] private TextMeshPro m_remainingTurnTexts;
        /// <summary>
        /// The game controller.
        /// <para>Used to subscribe to the new turn and notify of the master arrival.</para>
        /// </summary>
        [SerializeField] private GameController m_controller;
        /// <summary>
        /// The array of result from the <see cref="Physics2D.OverlapCircleNonAlloc(Vector2,float,Collider2D[],int)"/>.
        /// <para>Used to find nearby cats.</para>
        /// </summary>
        private readonly Collider2D[] m_circleCastColliders = new Collider2D[8];
        /// <summary>
        /// The transform of the master.
        /// <para>Used to launch the overlap circle cast.</para>
        /// </summary>
        private Transform m_transform;
        /// <summary>
        /// The threshold before the master is considered not moving.
        /// </summary>
        private const float VELOCITY_THRESHOLD = 0.005f;
        /// <summary>
        /// The number of turn remaining.
        /// <para>Used to display them on the master.</para>
        /// </summary>
        private int RemainingTurn;
        
        private void Start()
        {
            m_transform = transform;
            if (m_rigidBody == null)
                m_rigidBody = GetComponent<Rigidbody2D>();
            if (m_remainingTurnTexts == null)
                m_remainingTurnTexts = GetComponentInChildren<TextMeshPro>();
            if (m_controller == null)
                m_controller = FindObjectOfType<GameController>();
            
            m_controller.SubscribeToNewTurnEvent(OnNewTurn);
            RemainingTurn = m_controller.GetTurnNumberAfterMasterArrival();
            m_remainingTurnTexts.text = RemainingTurn.ToString();
        }

        public void OnNewTurn()
        {
            RemainingTurn--;
            m_remainingTurnTexts.text = RemainingTurn.ToString();
        }

        /// <summary>
        /// Launch the master, who will throw himself toward the nearest cat.
        /// </summary>
        public void SendMaster()
        {
            //First call a OverlapCircleNonAlloc to get all the cats in the vicinity, using the cat transform, and the ability radius.
            Vector3 l_pos = m_transform.position;
            int l_nbColl = Physics2D.OverlapCircleNonAlloc(l_pos, m_radius,m_circleCastColliders,1<<LayerMask.NameToLayer("Cat"));
            //If there is no cat, return
            if (l_nbColl == 0)
                return;
            //Set the nearest distance to the max, just to be sure
            float l_nearestDist = float.MaxValue;
            Collider2D l_nearestCat = null;
            //For each collision that is not this cat or null
            for (int i = 0; i < m_circleCastColliders.Length ; i++)
            {
                if(m_circleCastColliders[i] == null || m_circleCastColliders[i].gameObject.GetComponent<Cat>().IsStunned())
                    continue;
        
                //Check the cat distance, and if it is closer to this cat than the current nearest one,
                //Set it as the nearest one
                float l_dist = (m_transform.position - m_circleCastColliders[i].transform.position).sqrMagnitude;
                if (l_dist<l_nearestDist) {
                    l_nearestDist=l_dist;
                    l_nearestCat=m_circleCastColliders[i];
                }
            }

            //If we got a cat in the given radius, launch the master toward it
            if (l_nearestCat)
            {
                Vector2 l_dir = (l_nearestCat.transform.position - transform.position).normalized;
                m_rigidBody.AddForce(l_dir * m_force);
            }
        }

        public bool IsMoving()
        {
            if (m_rigidBody.velocity.sqrMagnitude > VELOCITY_THRESHOLD)
            {
                return true;
            }
            m_rigidBody.velocity = Vector3.zero;

            return false;
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            //Nothing special should be done when it is not our turn or we are stunned
            if ((other.gameObject.layer != LayerMask.NameToLayer("Cat") && other.gameObject.layer != LayerMask.NameToLayer("Damageable")))
                return;

            if (other.gameObject.layer == LayerMask.NameToLayer("Cat"))
            {
                //Get the cat component of the other cat
                Cat l_cat = other.gameObject.GetComponent<Cat>();
				
                //If the cat is stunned, do not do damage
                if (l_cat.IsStunned())
                    return;
            }
			//Get the health component of the cat
            Health l_health = other.gameObject.GetComponent<Health>();
			
            //Deal tge damages to the cat based
            int l_damage = Mathf.FloorToInt(m_damage);
            l_health.DealDamage(l_damage, DamageType.master);
        }
    }
}
