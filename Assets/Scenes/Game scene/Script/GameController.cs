﻿using Scenes.General.Scripts;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace Scenes.Game_scene.Script
{
	/// <summary>
	/// Handle the behaviour of a game.
	/// </summary>
	[DisallowMultipleComponent]
	public class GameController : MonoBehaviour
	{
		/// <summary>
		/// The text showing the player one score.
		/// </summary>
		[SerializeField] private TextMeshProUGUI m_playerOneScoreText;
		/// <summary>
		/// The text showing the player two score.
		/// </summary>
		[SerializeField] private TextMeshProUGUI m_playerTwoScoreText;
		/// <summary>
		/// The on new turn event.
		/// </summary>
		[SerializeField] private UnityEvent m_onNewTurn;
		/// <summary>
		/// The layers on which the preview should collide.
		/// </summary>
		[SerializeField] private LayerMask m_previewLayers;
		/// <summary>
		/// The layers on which the preview should collide if the cat has the ghost ability.
		/// </summary>
		[SerializeField] private LayerMask m_previewLayersGhost;
		/// <summary>
		/// The number of turn between each master attack.
		/// </summary>
		[SerializeField] private int m_turnBetweenMasterAttack;
		/// <summary>
		/// The number of turns remaining after the master arrived.
		/// </summary>
		[SerializeField] private int m_turnsAfterMasterArrival = 1;
		/// <summary>
		/// The score of the player one.
		/// </summary>
		private ushort m_playerOneScore;
		/// <summary>
		/// The score of the player two.
		/// </summary>
		private ushort m_playerTwoScore;
		/// <summary>
		/// Whether or not the master is there.
		/// </summary>
		private bool m_masterIsHere;
		/// <summary>
		/// The number of turn since the last time the master attacked.
		/// </summary>
		private int m_turnSinceMasterAttacked;
		/// <summary>
		/// The data manager.
		/// <para>Used to set the score at the end of the game.</para>
		/// </summary>
		private DataManager m_dataManager;
		
		private void Start()
		{
			m_dataManager = FindObjectOfType<DataManager>();

			//Initialize both score texts to 0
			m_playerOneScoreText.text = "0";
			m_playerTwoScoreText.text = "0";

			//Since the master arrive at the very end of the turn
			m_turnsAfterMasterArrival*=2;
			m_turnsAfterMasterArrival++;
		}

		public LayerMask GetPreviewLayers(bool p_ghost)
		{
			return p_ghost ? m_previewLayersGhost : m_previewLayers;
		}

		public int GetTurnNumberAfterMasterArrival()
		{
			return m_turnsAfterMasterArrival;
		}
		
		public int GetTurnBetweenMasterAttacks()
		{
			return m_turnBetweenMasterAttack;
		}
		
		public void NewTurn()
		{
			m_onNewTurn.Invoke();
			if (m_masterIsHere)
				m_turnsAfterMasterArrival--;

			if(m_turnsAfterMasterArrival == 0)
				EndGame();
		}

		public void StartCountdown()
		{
			m_masterIsHere = true;
		}

		public bool IsMasterHere()
		{
			return m_masterIsHere;
		}
		
		/// <summary>
		/// Subscribe the given unityaction to the new turn event.
		/// </summary>
		/// <param name="p_action">The method we wish to invoke each time the event is thrown.</param>
		public void SubscribeToNewTurnEvent(UnityAction p_action)
		{
			m_onNewTurn.AddListener(p_action);
		}

		/// <summary>
		/// increase the score of the players.
		/// </summary>
		/// <param name="p_playerNumber">The team of which we want to increase the score.</param>
		/// <param name="p_score">The amount we wish to increase the score by.</param>
		public void IncreaseScore(int p_playerNumber, ushort p_score)
		{
			if (p_playerNumber == 1)
			{
				m_playerOneScore += p_score;
				m_playerOneScoreText.text = m_playerOneScore.ToString();
			}
			else
			{
				m_playerTwoScore += p_score;
				m_playerTwoScoreText.text = m_playerTwoScore.ToString();
			}
		}

		public int GetPlayerOneScore()
		{
			return m_playerOneScore;
		}
		
		public int GetPlayerTwoScore()
		{
			return m_playerTwoScore;
		}

		/// <summary>
		/// Save the scores in the data manager and load the endgame scene.
		/// </summary>
		public void EndGame()
		{
			m_dataManager.SetPlayerOneScore(m_playerOneScore);
			m_dataManager.SetPlayerTwoScore(m_playerTwoScore);
			
			SceneManager.LoadScene("EndGame");
		}
	}
}
