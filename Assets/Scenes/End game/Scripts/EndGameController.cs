﻿using Scenes.General.Scripts;
using TMPro;
using UnityEngine;

namespace Scenes.End_game.Scripts
{
	/// <summary>
	/// Handle the behavior of the end game scene.
	/// </summary>
	[DisallowMultipleComponent]
	public class EndGameController : MonoBehaviour
	{
		/// <summary>
		/// The text showing the winning team.
		/// </summary>
		[SerializeField] private TextMeshProUGUI m_winner;
		/// <summary>
		/// The text showing the score of the player one team.
		/// </summary>
		[SerializeField] private TextMeshProUGUI m_playerOneScore;
		/// <summary>
		/// The text showing the score of the player two team.
		/// </summary>
		[SerializeField] private TextMeshProUGUI m_playerTwoScore;
		
		void Start()
		{
			//Get the data manager to get the cores
			DataManager l_dataManager = FindObjectOfType<DataManager>();

			ushort l_playerOneScore = l_dataManager.GetPlayerOneScore();
			ushort l_playerTwoScore = l_dataManager.GetPlayerTwoScore();

			//Set the texts according to the scores
			if (l_playerOneScore > l_playerTwoScore)
			{
				m_winner.text = "Blue team win !";
			}
			else if(l_playerOneScore < l_playerTwoScore)
			{
				m_winner.text = "Red team win !";
			}
			else
			{
				m_winner.text = "It's a tie !";
			}

			m_playerOneScore.text = l_playerOneScore.ToString();
			m_playerTwoScore.text = l_playerTwoScore.ToString();
		}
	}
}
