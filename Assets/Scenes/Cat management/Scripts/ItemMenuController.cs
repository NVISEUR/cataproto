﻿using System;
using Game_data.Items.Scripts;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Scenes.Cat_management.Scripts
{
    [RequireComponent(typeof(RectTransform))]
    public class ItemMenuController : MonoBehaviour, IDragHandler, IEndDragHandler, IPointerClickHandler, IBeginDragHandler
    {
        /// <summary>
        /// The rect transform of this object.
        /// </summary>
        private RectTransform m_transform;
        /// <summary>
        /// The initial parent of the item object.
        /// <para>Used to reset it to it's initial position if needed.</para>
        /// </summary>
        private RectTransform m_initialParent;
        /// <summary>
        /// The canvas.
        /// <para>Used to change the item object parent when moving it around.</para>
        /// </summary>
        private RectTransform m_canvas;
        /// <summary>
        /// The menu controller.
        /// <para>Used to select/unselect the item</para>
        /// </summary>
        private MenuController m_controller;
        /// <summary>
        /// The data of this item object.
        /// </summary>
        private Item m_item;
        /// <summary>
        /// The item details panel.
        /// <para>Used to show it on click.</para>
        /// </summary>
        private ItemDetails m_details;
        /// <summary>
        /// Whether or not we are dragging the item.
        /// <para>Needed to avoid triggering the pointer click if not needed.</para>
        /// </summary>
        private bool m_dragging;
        
        private void Awake()
        {
            m_transform = GetComponent<RectTransform>();
            m_initialParent = m_transform.parent as RectTransform;
            m_canvas = FindObjectOfType<Canvas>().transform as RectTransform;
            m_controller = FindObjectOfType<MenuController>();
        }

        /// <summary>
        /// Set the data of this item object.
        /// </summary>
        /// <param name="p_item">The item data</param>
        public void SetItem(Item p_item)
        {
            m_item = p_item;
            GetComponent<RawImage>().texture = m_item.GetIcon();
        }

        /// <summary>
        /// Set the item details panel.
        /// </summary>
        /// <param name="p_details">The item details panel.</param>
        public void SetItemDetails(ItemDetails p_details)
        {
            m_details = p_details;
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            if(m_transform.parent != m_canvas)
                m_transform.SetParent(m_canvas,true);

            m_dragging = true;
        }

        public void OnDrag(PointerEventData eventData)
        {
            m_transform.position = Input.mousePosition;
        }

        /// <summary>
        /// Reset the parent of this gameobject.
        /// </summary>
        public void Unselect()
        {
            m_transform.SetParent(m_initialParent);
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            //Try to select the item and return whether or not it has been selected.
            bool l_selected = m_controller.SelectItem(this,Input.mousePosition);

            m_dragging = false;

            //If it has not been selected (or has been deselected),
            //We reset the parent and unselect the item if needed.
            if (!l_selected)
            {
                m_transform.SetParent(m_initialParent);

                switch (m_item.GetType())
                {
                    case ItemType.collar:
                        if (m_controller.GetSelectedCollar() == this)
                            m_controller.Unselect(m_item.GetType());
                        break;
                    case ItemType.cloak:
                        if (m_controller.GetSelectedCloak() == this)
                            m_controller.Unselect(m_item.GetType());
                        break;
                    case ItemType.hat:
                        if (m_controller.GetSelectedHat() == this)
                            m_controller.Unselect(m_item.GetType());
                        break;
                }

                if (m_controller.GetCurrentType() != m_item.GetType())
                {
                    gameObject.SetActive(false);
                }
                
            }
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            //If we have been dragging the item, do not show the details.
            if (m_dragging)
                return;

            string l_effectOne = "";
            string l_effectTwo = "";

            //Get the effects as a readable string.
            if (Math.Abs(m_item.GetFirstEffect().GetValue()) > float.Epsilon)
            {
                ItemEffect l_effect = m_item.GetFirstEffect();
    
                if (l_effect != null)
                {
                    switch (l_effect.GetType())
                    {
                        case ItemEffectType.ChangeDamage:
                            l_effectOne = ((l_effect.GetValue() > 0) ? "Increase" : "Decrease") + " damage done by "+l_effect.GetValue();
                            break;
                        case ItemEffectType.ChangeHealth:
                            l_effectOne = ((l_effect.GetValue() > 0) ? "Increase" : "Decrease") + " the maximum health by "+l_effect.GetValue();
                            break;
                        case ItemEffectType.ChangeSize:
                            l_effectOne = ((l_effect.GetValue() > 0) ? "Increase" : "Decrease") + " size by "+l_effect.GetValue();
                            break;
                        case ItemEffectType.ChangeForce:
                            l_effectOne = ((l_effect.GetValue() > 0) ? "Increase" : "Decrease") + " speed by "+l_effect.GetValue();
                            break;
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
    
                if (Math.Abs(m_item.GetSecondEffect().GetValue()) > float.Epsilon)
                {
                    l_effect = m_item.GetSecondEffect();
    
                    if (l_effect != null)
                    {
                        switch (l_effect.GetType())
                        {
                            case ItemEffectType.ChangeDamage:
                                l_effectTwo = ((l_effect.GetValue() > 0) ? "Increase" : "Decrease") + " damage done by "+l_effect.GetValue();
                                break;
                            case ItemEffectType.ChangeHealth:
                                l_effectTwo = ((l_effect.GetValue() > 0) ? "Increase" : "Decrease") + " the maximum health by "+l_effect.GetValue();
                                break;
                            case ItemEffectType.ChangeSize:
                                l_effectTwo = ((l_effect.GetValue() > 0) ? "Increase" : "Decrease") + " size by "+l_effect.GetValue();
                                break;
                            case ItemEffectType.ChangeForce:
                                l_effectTwo = ((l_effect.GetValue() > 0) ? "Increase" : "Decrease") + " speed by "+l_effect.GetValue();
                                break;
                            default:
                                throw new ArgumentOutOfRangeException();
                        }
                    }
                }
            }
            //Initialize the details panel and show it.
            m_details.Initialize(m_item.GetIcon(),m_item.GetName(),l_effectOne,l_effectTwo,m_item.GetDescription());
            m_details.gameObject.SetActive(true);
        }
    }
}
