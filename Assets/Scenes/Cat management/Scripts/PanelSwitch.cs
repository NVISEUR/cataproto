﻿using UnityEngine;

namespace Scenes.Cat_management.Scripts
{
	/// <summary>
	/// Handle the behavior of the deck/item menu regarding to panel switching
	/// </summary>
	public class PanelSwitch : MonoBehaviour
	{
		/// <summary>
		/// The animator.
		/// <para>Used to launch the change screen animations.</para>
		/// </summary>
		[SerializeField] private Animator m_animator;
		/// <summary>
		/// The rect of the left trigger.
		/// <para>Used to know if we are clicking on it.</para>
		/// </summary>
		[SerializeField] private RectTransform m_leftTrigger;
		/// <summary>
		/// The rect of the right trigger.
		/// <para>Used to know if we are clicking on it.</para>
		/// </summary>
		[SerializeField] private RectTransform m_rightTrigger;
		/// <summary>
		/// The rect of the first screen "preview".
		/// <para>Used to know if we are clicking on it.</para>
		/// </summary>
		[SerializeField] private RectTransform m_firstScreenPreview;
		/// <summary>
		/// The rect of the second screen "preview".
		/// <para>Used to know if we are clicking on it.</para>
		/// </summary>
		[SerializeField] private RectTransform m_secondScreenPreview;
		/// <summary>
		/// Whether or not we are currently looking at the deck panel.
		/// </summary>
		private bool m_deck = true;
		/// <summary>
		/// The hash representing the SwipeToEquip animator parameter.
		/// </summary>
		private static readonly int m_swipeToEquip = Animator.StringToHash("SwipeToEquip");
		/// <summary>
		/// The hash representing the SwipeToDeck animator parameter.
		/// </summary>
		private static readonly int m_swipeToDeck = Animator.StringToHash("SwipeToDeck");
		/// <summary>
		/// The menu controller.
		/// <para>Used to refresh the item panel cats when going to it.</para>
		/// </summary>
		private MenuController m_controller;
	
		private void Start()
		{
			m_leftTrigger.gameObject.SetActive(false);
			m_controller = GetComponent<MenuController>();
		}

		private void Update()
		{
			//If we are releasing the left click above one of the "preview" or one of the trigger.
			//We change the active trigger and set the animator parameters to change screen.
			if (Input.GetMouseButtonUp(0))
			{
				if (RectTransformUtility.RectangleContainsScreenPoint(m_firstScreenPreview, Input.mousePosition)
				    || RectTransformUtility.RectangleContainsScreenPoint(m_secondScreenPreview, Input.mousePosition))
				{
					if (m_deck)
					{
						m_leftTrigger.gameObject.SetActive(true);
						m_rightTrigger.gameObject.SetActive(false);
						m_animator.SetBool(m_swipeToDeck,false);
						m_animator.SetBool(m_swipeToEquip,true);
						m_deck = !m_deck;
						m_controller.RefreshItemPanelCats();
					}
					else
					{
						m_leftTrigger.gameObject.SetActive(false);
						m_rightTrigger.gameObject.SetActive(true);
						m_animator.SetBool(m_swipeToEquip,false);
						m_animator.SetBool(m_swipeToDeck,true);
						m_deck = !m_deck;
					}
				}
			
				if (m_deck && RectTransformUtility.RectangleContainsScreenPoint(m_rightTrigger,Input.mousePosition))
				{
					m_leftTrigger.gameObject.SetActive(true);
					m_rightTrigger.gameObject.SetActive(false);
					m_animator.SetBool(m_swipeToDeck,false);
					m_animator.SetBool(m_swipeToEquip,true);
					m_deck = !m_deck;
					m_controller.RefreshItemPanelCats();
				}
				else if(!m_deck && RectTransformUtility.RectangleContainsScreenPoint(m_leftTrigger,Input.mousePosition))
				{
					m_leftTrigger.gameObject.SetActive(false);
					m_rightTrigger.gameObject.SetActive(true);
					m_animator.SetBool(m_swipeToEquip,false);
					m_animator.SetBool(m_swipeToDeck,true);
					m_deck = !m_deck;
				}
			}
		}
	}
}
