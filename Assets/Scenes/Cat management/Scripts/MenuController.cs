﻿using System.Collections.Generic;
using System.Linq;
using Game_data.Cats.Scripts;
using Game_data.Items.Scripts;
using Scenes.General.Scripts;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Scenes.Cat_management.Scripts
{
    /// <summary>
    /// Represent a deck of cats.
    /// <para>A deck is composed of four cats, each owning up to three items.</para>
    /// </summary>
    public class Deck
    {
        /// <summary>
        /// The name of the deck.
        /// </summary>
        private string m_name;
        /// <summary>
        /// The cats of the deck.
        /// </summary>
        private DeckCat[] m_cats = new DeckCat[4];

        public Deck(string p_name)
        {
            m_name = p_name;
        }

        public string GetName()
        {
            return m_name;
        }

        public void SetName(string p_name)
        {
            m_name = p_name;
        }
        
        public DeckCat[] GetCats()
        {
            return m_cats;
        }

        public void AddCat(DeckCat p_cat, int p_pos)
        {
            if (!m_cats.Contains(p_cat))
            {
                m_cats[p_pos] = p_cat;
            }
        }

        /// <summary>
        /// Try to remove the cat in the given slot.
        /// </summary>
        /// <param name="p_pos">The cat slot.</param>
        public void RemoveCat(int p_pos)
        {
            m_cats[p_pos] = null;
        }

        /// <summary>
        /// Get the number of cats currently in the deck.
        /// </summary>
        /// <returns>The number of cats currently in the deck.</returns>
        public int NumberOfCats()
        {
            return m_cats.Count(cat => cat != null);
        }
    }

    /// <summary>
    /// Represent a cat in the deck, with all it's items.
    /// </summary>
    public class DeckCat
    {
        /// <summary>
        /// The cat data.
        /// </summary>
        private CatData m_data;
        /// <summary>
        /// The cat hat id.
        /// </summary>
        private short m_hat = -1;
        /// <summary>
        /// The cat collar id.
        /// </summary>
        private short m_collar = -1;
        /// <summary>
        /// The cat cloak id.
        /// </summary>
        private short m_cloak = -1;

        public DeckCat(CatData p_data)
        {
            m_data = p_data;
        }

        public CatData GetData()
        {
            return m_data;
        }

        public short GetHatId()
        {
            return m_hat;
        }

        public short GetCollarId()
        {
            return m_collar;
        }

        public short GetCloakId()
        {
            return m_cloak;
        }

        public void SetHatId(short p_hat)
        {
            m_hat = p_hat;
        }

        public void SetCollarId(short p_collar)
        {
            m_collar = p_collar;
        }

        public void SetCloakId(short p_cloak)
        {
            m_cloak = p_cloak;
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            
            if (obj.GetType() != GetType())
                return false;
            
            return ((DeckCat)obj).GetData().Equals(GetData());
        }

        public override int GetHashCode()
        {
            // ReSharper disable once BaseObjectGetHashCodeCallInGetHashCode
            return base.GetHashCode();
        }
    }

    /// <summary>
    /// Handle the behavior of the cat menu.
    /// </summary>
    public class MenuController : MonoBehaviour
    {
        /// <summary>
        /// The data of each cats.
        /// </summary>
        [SerializeField] private CatData[] m_cats;

        /// <summary>
        /// The menu image.
        /// <para>Used to show the current cat</para>
        /// </summary>
        [SerializeField] private RawImage m_catImage;

        /// <summary>
        /// The prefab of the items.
        /// </summary>
        [SerializeField] private GameObject m_itemPrefab;

        /// <summary>
        /// The scroll rect content.
        /// <para>Used to store the items.</para>
        /// </summary>
        [SerializeField] private Transform m_scrollContent;

        /// <summary>
        /// The list containing all the items.
        /// </summary>
        [SerializeField] private ItemList m_items;

        /// <summary>
        /// The scene manager.
        /// <para>Used to go back to the main menu.</para>
        /// </summary>
        [SerializeField] private SceneSwitcher m_sceneManager;

        /// <summary>
        /// The item details panel.
        /// </summary>
        [SerializeField] private ItemDetails m_itemDetails;

        /// <summary>
        /// The scroll rect content transform.
        /// </summary>
        [SerializeField] private RectTransform m_catScrollContent;

        /// <summary>
        /// The first selected cat square transform.
        /// </summary>
        [SerializeField] private RectTransform[] m_CatRects = new RectTransform[4];

        /// <summary>
        /// The canvas transform.
        /// </summary>
        [SerializeField] private RectTransform m_canvas;

        /// <summary>
        /// The prefab of the cat selection object.
        /// </summary>
        [SerializeField] private GameObject m_catSelectPrefab;

        /// <summary>
        /// The cat details gameObject
        /// </summary>
        [SerializeField] private CatDetails m_catDetails;

        /// <summary>
        /// The text of the next deck button.
        /// </summary>
        [SerializeField] private Text m_nextDeckText;

        /// <summary>
        /// The previous deck button.
        /// </summary>
        [SerializeField] private Button m_previousDeck;

        /// <summary>
        /// The rect of the cloak slot.
        /// </summary>
        [SerializeField] private RectTransform m_cloakRect;

        /// <summary>
        /// The rect of the hat slot.
        /// </summary>
        [SerializeField] private RectTransform m_hatRect;

        /// <summary>
        /// The rect of the collar slot.
        /// </summary>
        [SerializeField] private RectTransform m_collarRect;
        
        /// <summary>
        /// The deck title/input field.
        /// <para>Used to rename the deck.</para>
        /// </summary>
        [SerializeField] private TMP_InputField m_inputField;

        /// <summary>
        /// The delete deck button.
        /// </summary>
        [SerializeField] private Button m_deleteButton;
        
        /// <summary>
        /// All the hats as gameobjects.
        /// </summary>
        private List<GameObject> m_loadedHats;

        /// <summary>
        /// All the collars as gameobjects.
        /// </summary>
        private List<GameObject> m_loadedCollars;

        /// <summary>
        /// All the cloaks as gameobjects.
        /// </summary>
        private List<GameObject> m_loadedCloaks;

        /// <summary>
        /// The selected cat Id.
        /// </summary>
        private int m_selectCatId;

        /// <summary>
        /// The currently selected slot.
        /// <para>This is the slot corresponding to the current type of items we are browsing.</para>
        /// </summary>
        private RectTransform m_selectedSlot;

        /// <summary>
        /// The currently selected hat.
        /// </summary>
        private ItemMenuController m_selectedHat;

        /// <summary>
        /// The currently selected collar.
        /// </summary>
        private ItemMenuController m_selectedCollar;

        /// <summary>
        /// The currently selected cloak.
        /// </summary>
        private ItemMenuController m_selectedCloak;

        /// <summary>
        /// The type of items we are currently browsing.
        /// </summary>
        private ItemType m_currentItems;

        /// <summary>
        /// The data manager.
        /// <para>Used to select cats.</para>
        /// </summary>
        private DataManager m_dataManager;

        /// <summary>
        /// Whether or not each slot has been filled by a cat.
        /// </summary>
        private bool[] m_catSelected = new bool[4];

        /// <summary>
        /// the decks of the player.
        /// </summary>
        private List<Deck> m_playerDecks = new List<Deck>();

        /// <summary>
        /// The currently selected deck.
        /// </summary>
        private byte m_selectedDeck;

        /// <summary>
        /// The details objects of the cats.
        /// </summary>
        private Dictionary<int, GameObject> m_catDetailses = new Dictionary<int, GameObject>();

        /// <summary>
        /// The details objects of the selected cats.
        /// </summary>
        private List<GameObject> m_selectedCats = new List<GameObject>();

        private void Start()
        {
            m_playerDecks.Add(new Deck("My first deck"));
            m_inputField.text = m_playerDecks[m_selectedDeck].GetName();

            if (m_selectedDeck == m_playerDecks.Count - 1)
                m_nextDeckText.text = "+>";
            else
                m_nextDeckText.text = ">";

            if (m_selectedDeck == 0)
            {
                m_previousDeck.interactable = false;
                m_deleteButton.interactable = false;
            }

            //initialize the scroll rect with the gameobjects, using the cats scripatble object
            for (int i = 0; i < m_cats.Length; i++)
            {
                GameObject l_cat = Instantiate(m_catSelectPrefab, m_catScrollContent);
                l_cat.GetComponent<CatSelect>().Initialize(this, m_cats[i], m_CatRects, m_canvas, true, m_catDetails);
                m_catDetailses.Add(m_cats[i].GetId(), l_cat);
            }

            m_dataManager = FindObjectOfType<DataManager>();
            m_inputField.onEndEdit.AddListener(delegate { ChangeDeckName();});
        }

        public void ChangeDeckName()
        {
            m_playerDecks[m_selectedDeck].SetName(m_inputField.text);
        }

        public void DeleteDeck()
        {
            m_playerDecks.Remove(m_playerDecks[m_selectedDeck]);
            PreviousDeck();
        }

        /// <summary>
        /// Select the next deck in the list and show it on screen.
        /// </summary>
        public void NextDeck()
        {
            if (m_selectedDeck == m_playerDecks.Count - 1)
            {
                m_playerDecks.Add(new Deck("New Deck"+m_playerDecks.Count));
                m_selectedDeck++;
            }
            else
            {
                m_selectedDeck++;
                if (m_selectedDeck == m_playerDecks.Count - 1)
                    m_nextDeckText.text = "+>";
                else
                    m_nextDeckText.text = ">";
            }

            if (m_previousDeck.interactable == false)
            {
                m_previousDeck.interactable = true;
            }

            if (m_deleteButton.interactable == false)
            {
                m_deleteButton.interactable = true;
            }

            m_inputField.text = m_playerDecks[m_selectedDeck].GetName();
            
            RefreshDeckSelection();
        }

        /// <summary>
        /// Select the preview deck in the list and show it on screen.
        /// </summary>
        public void PreviousDeck()
        {
            m_selectedDeck--;

            if (m_selectedDeck == 0)
            {
                m_previousDeck.interactable = false;
                m_deleteButton.interactable = false;
            }
            
            if (m_selectedDeck == m_playerDecks.Count - 1)
                m_nextDeckText.text = "+>";
            else
                m_nextDeckText.text = ">";
            
            m_inputField.text = m_playerDecks[m_selectedDeck].GetName();
            
            RefreshDeckSelection();
        }

        /// <summary>
        /// Remove all selected cats from the deck screen before adding the ones in the newly selected deck.
        /// </summary>
        private void RefreshDeckSelection()
        {
            for (int i = 0;i < m_catSelected.Length;i++)
            {
                m_catSelected[i] = false;
            }
            m_dataManager.UnselectAllCat();
            
            for (int i = 0; i < m_selectedCats.Count; i++)
            {
                m_selectedCats[i].GetComponent<CatSelect>().Unselect();
            }
            m_selectedCats.Clear();
            
            if (m_playerDecks[m_selectedDeck].NumberOfCats() > 0)
            {
                for (int i = 0; i < m_playerDecks[m_selectedDeck].GetCats().Length; i++)
                {
                    if(m_playerDecks[m_selectedDeck].GetCats()[i] == null)
                        continue;
                    
                    DeckCat l_cat = m_playerDecks[m_selectedDeck].GetCats()[i];
                    if (l_cat != null)
                    {
                        GameObject m_object;
                        m_catDetailses.TryGetValue(l_cat.GetData().GetId(), out m_object);
                        SelectCat(l_cat.GetData(), i,m_object);
                    }
                }
            }
        }

        /// <summary>
        /// Add the given cat to the data manager and increase the counter if the adding was successful.
        /// </summary>
        /// <param name="p_cat">The cat we wish to select.</param>
        /// <param name="p_rect">The index of the rect we are trying to put the cat in.</param>
        /// <param name="p_obj">The gameobject representing the cat in the menu.</param>
        /// <returns>True if the cat was selected.</returns>
        public bool SelectCat(CatData p_cat, int p_rect, GameObject p_obj)
        {
            CatData l_secondCat = null;
            GameObject l_secondObject = null;
            int l_otherRect = -1;

            //First, remove the cat in the slot if one is already there
            if (m_catSelected[p_rect])
            {
                l_secondCat = m_playerDecks[m_selectedDeck].GetCats()[p_rect].GetData();
                m_catDetailses.TryGetValue(l_secondCat.GetId(), out l_secondObject);
                UnselectCat(l_secondCat,p_rect,l_secondObject);
            }
            //Remove the cat we are moving from his actual slot if he's already selected
            if (m_dataManager.IsSelectd(p_cat.GetId()))
            {
                l_otherRect = p_obj.GetComponent<CatSelect>().GetSlot();
                UnselectCat(p_cat,l_otherRect,p_obj);
            }
            //If the current cat was already in another slot, reselect the first one into that slot
            if (l_otherRect != -1 && l_secondCat != null && l_secondObject != null)
            {
                bool l_addedSecond = m_dataManager.SelectCat(l_secondCat.GetId());

                if (l_addedSecond)
                {
                    m_playerDecks[m_selectedDeck].AddCat(new DeckCat(l_secondCat), l_otherRect);
                    m_catSelected[l_otherRect] = true;
                    m_selectedCats.Add(l_secondObject);
                    l_secondObject.transform.SetParent(m_CatRects[l_otherRect]);
                    l_secondObject.transform.localPosition = Vector3.zero;
                    
                    l_secondObject.GetComponent<CatSelect>().SetSlot(l_otherRect);
                }
            }

            bool l_added = m_dataManager.SelectCat(p_cat.GetId());
            //Finally put the first one into the new slot
            if (l_added)
            {
                m_playerDecks[m_selectedDeck].AddCat(new DeckCat(p_cat), p_rect);
                m_catSelected[p_rect] = true;
                m_selectedCats.Add(p_obj);
                p_obj.transform.SetParent(m_CatRects[p_rect]);
                p_obj.transform.localPosition = Vector3.zero;
            }
            
            return l_added;
        }

        /// <summary>
        /// Remove the given cat to the data manager and reduce the counter if the removing was successful.
        /// </summary>
        /// <param name="p_cat">The cat we wish to unselect.</param>
        /// <param name="p_rect">The index of the rect we are trying to remove the cat from.</param>
        /// <param name="p_obj">The gameobject representing the cat in the menu.</param>
        public void UnselectCat(CatData p_cat, int p_rect, GameObject p_obj)
        {
            if (!m_dataManager.IsSelectd(p_cat.GetId()))
                return;
            
            bool l_unselected = m_dataManager.UnselectCat(p_cat.GetId());

            if (l_unselected)
            {
                m_catSelected[p_rect] = false;
                m_playerDecks[m_selectedDeck].RemoveCat(p_rect);
                m_selectedCats.Remove(p_obj);
                p_obj.GetComponent<CatSelect>().Unselect();
            }
        }

        private void Awake()
        {
            m_loadedHats = new List<GameObject>();
            m_loadedCollars = new List<GameObject>();
            m_loadedCloaks = new List<GameObject>();

            //Initialize all the items and put them in the scroll rect.
            for (int i = 0; i < m_items.GetHats().Length; i++)
            {
                GameObject l_object = Instantiate(m_itemPrefab,m_scrollContent);
                l_object.SetActive(false);
                l_object.name = "HAT";
                m_loadedHats.Add(l_object);
                ItemMenuController l_controller = l_object.GetComponent<ItemMenuController>();
                l_controller.SetItem(m_items.GetHats()[i]);
                l_controller.SetItemDetails(m_itemDetails);
            }
            
            for (int i = 0; i < m_items.GetCollars().Length; i++)
            {
                GameObject l_object = Instantiate(m_itemPrefab,m_scrollContent);
                l_object.SetActive(false);
                l_object.name = "COLLAR";
                m_loadedCollars.Add(l_object);
                ItemMenuController l_controller = l_object.GetComponent<ItemMenuController>();
                l_controller.SetItem(m_items.GetCollars()[i]);
                l_controller.SetItemDetails(m_itemDetails);
            }
            
            for (int i = 0; i < m_items.GetCloaks().Length; i++)
            {
                GameObject l_object = Instantiate(m_itemPrefab,m_scrollContent);
                l_object.SetActive(false);
                l_object.name = "CLOAK";
                m_loadedCloaks.Add(l_object);
                ItemMenuController l_controller = l_object.GetComponent<ItemMenuController>();
                l_controller.SetItem(m_items.GetCloaks()[i]);
                l_controller.SetItemDetails(m_itemDetails);
            }
        }

        private void Update()
        {
            //If we are pressing esc or the return button on android, go back to the main menu.
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                m_sceneManager.SwitchScene("MainMenu");
            }
        }

        /// <summary>
        /// Select the next cat.
        /// </summary>
        public void NextCat()
        {
            m_selectCatId++;
            if (m_selectCatId == m_playerDecks[m_selectedDeck].GetCats().Length)
                m_selectCatId = 0;
            if(m_playerDecks[m_selectedDeck].GetCats()[m_selectCatId] == null)
                NextCat();
            m_catImage.texture = m_playerDecks[m_selectedDeck].GetCats()[m_selectCatId].GetData().GetSprite().texture;
            RefreshItemPanelItems();
        }

        /// <summary>
        /// Select the previous cat.
        /// </summary>
        public void PreviousCat()
        {
            m_selectCatId--;
            if (m_selectCatId == -1)
                m_selectCatId = m_playerDecks[m_selectedDeck].GetCats().Length-1;
            if(m_playerDecks[m_selectedDeck].GetCats()[m_selectCatId] == null)
                PreviousCat();
            m_catImage.texture = m_playerDecks[m_selectedDeck].GetCats()[m_selectCatId].GetData().GetSprite().texture;
            RefreshItemPanelItems();
        }

        /// <summary>
        /// Select the first cat in the current deck in the item panel, and refresh the items list.
        /// </summary>
        public void RefreshItemPanelCats()
        {
            for (int i = 0; i < m_playerDecks[m_selectedDeck].GetCats().Length; i++)
            {
                if (m_playerDecks[m_selectedDeck].GetCats()[i] != null)
                {
                    m_catImage.texture = m_playerDecks[m_selectedDeck].GetCats()[i].GetData().GetSprite().texture;
                    m_selectCatId = i;
                    break;
                }
            }

            RefreshItemPanelItems();
        }

        /// <summary>
        /// Remove all the items from the cat slots, before filling them with the current cat items if any.
        /// </summary>
        private void RefreshItemPanelItems()
        {
            if (m_selectedHat != null)
            {
                m_selectedHat.Unselect();
                if (m_currentItems != ItemType.hat)
                    m_selectedHat.gameObject.SetActive(false);
                m_selectedHat = null;
            }
            
            if (m_selectedCloak != null)
            {
                m_selectedCloak.Unselect();
                if (m_currentItems != ItemType.cloak)
                    m_selectedCloak.gameObject.SetActive(false);
                m_selectedCloak = null;
            }
            
            if (m_selectedCollar != null)
            {
                m_selectedCollar.Unselect();
                if (m_currentItems != ItemType.collar)
                    m_selectedCollar.gameObject.SetActive(false);
                m_selectedCollar = null;
            }

            if (m_playerDecks[m_selectedDeck].GetCats()[m_selectCatId] != null)
            {
                if (m_playerDecks[m_selectedDeck].GetCats()[m_selectCatId].GetCloakId() != -1)
                {
                    m_selectedCloak = m_loadedCloaks[m_playerDecks[m_selectedDeck].GetCats()[m_selectCatId].GetCloakId()].GetComponent<ItemMenuController>();
                    Transform l_transform = m_selectedCloak.transform;
                    l_transform.SetParent(m_cloakRect);
                    l_transform.localPosition = Vector3.zero;
                    m_selectedCloak.gameObject.SetActive(true);
                }

                if (m_playerDecks[m_selectedDeck].GetCats()[m_selectCatId].GetHatId() != -1)
                {
                    m_selectedHat = m_loadedHats[m_playerDecks[m_selectedDeck].GetCats()[m_selectCatId].GetHatId()].GetComponent<ItemMenuController>();
                    Transform l_transform = m_selectedHat.transform;
                    l_transform.SetParent(m_hatRect);
                    l_transform.localPosition = Vector3.zero;
                    m_selectedHat.gameObject.SetActive(true);
                }

                if (m_playerDecks[m_selectedDeck].GetCats()[m_selectCatId].GetCollarId() != -1)
                {
                    m_selectedCollar = m_loadedCollars[m_playerDecks[m_selectedDeck].GetCats()[m_selectCatId].GetCollarId()].GetComponent<ItemMenuController>();
                    Transform l_transform = m_selectedCollar.transform;
                    l_transform.SetParent(m_collarRect);
                    l_transform.localPosition = Vector3.zero;
                    m_selectedCollar.gameObject.SetActive(true);
                }
            }
        }
        
        /// <summary>
        /// Select the given item if the mouse is released in the slot rect.
        /// </summary>
        /// <param name="p_item">The item we wish to select.</param>
        /// <param name="p_mousePos">The position of the mouse.</param>
        /// <returns>Whether or not the item has been selected.</returns>
        public bool SelectItem(ItemMenuController p_item, Vector3 p_mousePos)
        {
            Transform l_transform = null;
            if (RectTransformUtility.RectangleContainsScreenPoint(m_selectedSlot, p_mousePos))
            {
                switch (m_currentItems)
                {
                    case ItemType.cloak:
                        //Unselect the current cloak.
                        Unselect(ItemType.cloak);
                        //Then select the new one and get it's transform.
                        m_selectedCloak = p_item;
                        m_playerDecks[m_selectedDeck].GetCats()[m_selectCatId].SetCloakId((short)m_loadedCloaks.IndexOf(m_selectedCloak.gameObject));
                        l_transform = m_selectedCloak.transform;
                        break;
                    
                    case ItemType.hat:
                        //Unselect the current hat.
                        Unselect(ItemType.hat);
                        //Then select the new one and get it's transform.
                        m_selectedHat = p_item;
                        m_playerDecks[m_selectedDeck].GetCats()[m_selectCatId].SetHatId((short)m_loadedHats.IndexOf(m_selectedHat.gameObject));
                        l_transform = m_selectedHat.transform;
                        break;
                    
                    case ItemType.collar:
                        //Unselect the current collar.
                        Unselect(ItemType.collar);
                        //Then select the new one and get it's transform.
                        m_selectedCollar = p_item;
                        m_playerDecks[m_selectedDeck].GetCats()[m_selectCatId].SetCollarId((short)m_loadedCollars.IndexOf(m_selectedCollar.gameObject));
                        l_transform = m_selectedCollar.transform;
                        break;
                }
            }

            //If we got a transform, change it's parent to the selected slot and reset it's position.
            if (l_transform != null)
            {
                l_transform.SetParent(m_selectedSlot);
                l_transform.localPosition = Vector3.zero;
            }
            
            //Return true if we got a transform (if the item has been selected).
            return l_transform != null;
        }

        /// <summary>
        /// Unselect the item of the given type.
        /// </summary>
        /// <param name="p_type"></param>
        public void Unselect(ItemType p_type)
        {
            switch (p_type)
            {
                case ItemType.collar:
                    if (m_selectedCollar != null)
                    {
                        m_selectedCollar.Unselect();
                        m_selectedCollar = null;
                    }
                    break;
                case ItemType.cloak:
                    if (m_selectedCloak != null)
                    {
                        m_selectedCloak.Unselect();
                        m_selectedCloak = null;
                    }
                    break;
                case ItemType.hat:
                    if (m_selectedHat != null)
                    {
                        m_selectedHat.Unselect();
                        m_selectedHat = null;
                    }
                    break;
            }
        }

        /// <summary>
        /// Get the currently selected hat.
        /// </summary>
        /// <returns>The currently selected hat.</returns>
        public ItemMenuController GetSelectedHat()
        {
            return m_selectedHat;
        }
        
        /// <summary>
        /// Get the currently selected cloak.
        /// </summary>
        /// <returns>The currently selected cloak.</returns>
        public ItemMenuController GetSelectedCloak()
        {
            return m_selectedCloak;
        }

        /// <summary>
        /// Get the currently selected collar.
        /// </summary>
        /// <returns>The currently selected collar.</returns>
        public ItemMenuController GetSelectedCollar()
        {
            return m_selectedCollar;
        }

        /// <summary>
        /// Get the current type of items we are manipulating.
        /// </summary>
        /// <returns>The current type of item.</returns>
        public ItemType GetCurrentType()
        {
            return m_currentItems;
        }

        /// <summary>
        /// Deactivate the currently shown items and show the cloaks instead.
        /// </summary>
        public void GetCloaks()
        {
            m_selectedSlot = m_cloakRect;
            DeactivateItems();
            
            for (int i = 0; i < m_loadedCloaks.Count; i++)
            {
                m_loadedCloaks[i].SetActive(true);
            }

            m_currentItems = ItemType.cloak;
        }

        /// <summary>
        /// Deactivate the currently shown items and show the hats instead.
        /// </summary>
        public void GetHats()
        {
            m_selectedSlot = m_hatRect;
            DeactivateItems();
            
            for (int i = 0; i < m_loadedHats.Count; i++)
            {
                m_loadedHats[i].SetActive(true);
            }

            m_currentItems = ItemType.hat;
        }

        /// <summary>
        /// Deactivate the currently shown items and show the collars instead.
        /// </summary>
        public void GetCollars()
        {
            m_selectedSlot = m_collarRect;
            DeactivateItems();
            
            for (int i = 0; i < m_loadedCollars.Count; i++)
            {
                m_loadedCollars[i].SetActive(true);
            }

            m_currentItems = ItemType.collar;
        }

        /// <summary>
        /// Deactivate all the items of the current type.
        /// </summary>
        public void DeactivateItems()
        {
            switch (m_currentItems)
            {
                case ItemType.collar:
                    for (int i = 0; i < m_loadedCollars.Count; i++)
                    {
                        m_loadedCollars[i].SetActive(false);
                        if(m_selectedCollar != null)
                            m_selectedCollar.gameObject.SetActive(true);
                    }
                    break;
                
                case ItemType.hat:
                    for (int i = 0; i < m_loadedHats.Count; i++)
                    {
                        m_loadedHats[i].SetActive(false);
                        if(m_selectedHat != null)
                         m_selectedHat.gameObject.SetActive(true);
                    }
                    break;
                
                case ItemType.cloak:
                    for (int i = 0; i < m_loadedCloaks.Count; i++)
                    {
                        m_loadedCloaks[i].SetActive(false);
                        if(m_selectedCloak != null)
                            m_selectedCloak.gameObject.SetActive(true);
                    }
                    break;
            }
        }
    }
}
