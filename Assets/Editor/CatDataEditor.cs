﻿using Game_data.Abilities.Scripts;
using Game_data.Cats.Scripts;
using UnityEditor;

//A custom editor for the cat data, mainly for the special ability
namespace Editor
{
	[CustomEditor(typeof(CatData))]
	public class CatDataEditor : UnityEditor.Editor
	{
		//Boolean used for the foldout in the editor
		private static bool m_showAbility;
		//The cat data
		private SerializedProperty m_id;
		private SerializedProperty m_name;
		private SerializedProperty m_sprite;
		private SerializedProperty m_health;
		private SerializedProperty m_damage;
		private SerializedProperty m_initiative;
		private SerializedProperty m_force;
		private SerializedProperty m_weight;
		private SerializedProperty m_scale;
		//The ability data
		private SerializedProperty m_type;
		private SerializedProperty m_when;
		private SerializedProperty m_value;
		private SerializedProperty m_radius;
		private SerializedProperty m_gameObject;
		private SerializedProperty m_firstCollOnly;
		private SerializedProperty m_afterFirstColl;
		private SerializedProperty m_allEnemies;
		private SerializedProperty m_description;
	
		//Get all the members of the AbilityData as properties.
		void OnEnable()
		{
			// Setup the SerializedProperties.
			m_id = serializedObject.FindProperty("m_id");
			m_name = serializedObject.FindProperty("m_name");
			m_sprite = serializedObject.FindProperty("m_sprite");
			m_health = serializedObject.FindProperty("m_health");
			m_damage = serializedObject.FindProperty("m_damage");
			m_initiative = serializedObject.FindProperty("m_initiative");
			m_force = serializedObject.FindProperty("m_force");
			m_weight = serializedObject.FindProperty("m_weight");
			m_scale = serializedObject.FindProperty("m_scale");
		
			m_type = serializedObject.FindProperty("m_ability.m_type");
			m_when = serializedObject.FindProperty("m_ability.m_when");
			m_value = serializedObject.FindProperty("m_ability.m_value");
			m_radius = serializedObject.FindProperty("m_ability.m_radius");
			m_gameObject = serializedObject.FindProperty("m_ability.m_gameObject");
			m_firstCollOnly = serializedObject.FindProperty("m_ability.m_firstCollOnly");
			m_afterFirstColl = serializedObject.FindProperty("m_ability.m_afterFirstColl");
			m_allEnemies = serializedObject.FindProperty("m_ability.m_allEnemies");
			m_description = serializedObject.FindProperty("m_ability.m_description");
		}
	
		public override void OnInspectorGUI()
		{
			//First update the object
			serializedObject.Update ();
		
			//Draw the default inspector for all fields but the special ability
			EditorGUILayout.PropertyField(m_id);
			EditorGUILayout.PropertyField(m_name);
			EditorGUILayout.PropertyField(m_sprite);
			EditorGUILayout.PropertyField(m_health);
			EditorGUILayout.PropertyField(m_damage);
			EditorGUILayout.PropertyField(m_initiative);
			EditorGUILayout.PropertyField(m_force);
			EditorGUILayout.PropertyField(m_weight);
			EditorGUILayout.PropertyField(m_scale);

			//Add the foldout to store the ability
			m_showAbility = EditorGUILayout.Foldout(m_showAbility, "Ability");

			EditorGUI.indentLevel++;
		
			if (m_showAbility)
			{
			
				//Simply draw the two enum fields
				EditorGUILayout.PropertyField(m_type);
				EditorGUILayout.PropertyField(m_when);

				//Based on the type, draw the required other fields
				//The multiply damage ability only need the damage value
				if ((Type)m_type.enumValueIndex == Type.multiply_damage)
				{
					EditorGUILayout.PropertyField(m_value);
					EditorGUI.indentLevel++;
					EditorGUILayout.LabelField("(Base value + above value)");
					EditorGUI.indentLevel--;
				}
				//The throw projectile ability require the radius to check, the projectile object and whether 
				//we are targeting all the enemy in the radius or just the nearest.
				else if((Type)m_type.enumValueIndex == Type.throw_projectile)
				{
					EditorGUILayout.PropertyField(m_radius);
					EditorGUILayout.PropertyField(m_gameObject);
					EditorGUILayout.PropertyField(m_allEnemies);
				}
				//The zone damage ability require the radius and the damage value
				else if((Type)m_type.enumValueIndex == Type.zone_damage)
				{
					EditorGUILayout.PropertyField(m_radius);
					EditorGUILayout.PropertyField(m_value);
				}

				//For the on collision abilities,
				if ((When)m_when.enumValueIndex == When.onCatColl || (When)m_when.enumValueIndex == When.onWallColl
				    ||(When)m_when.enumValueIndex == When.onCatCollNonTurn)
				{
					//Only show the two possible boolean at the same time if none have been checked
					if (m_firstCollOnly.boolValue && !m_afterFirstColl.boolValue)
					{
						EditorGUILayout.PropertyField(m_firstCollOnly);
					}
					else if (m_afterFirstColl.boolValue && !m_firstCollOnly.boolValue)
					{
						EditorGUILayout.PropertyField(m_afterFirstColl);
					}
					else
					{
						EditorGUILayout.PropertyField(m_firstCollOnly);
						EditorGUILayout.PropertyField(m_afterFirstColl);
					}
				}
				EditorGUILayout.PropertyField(m_description);
			}

			EditorGUI.indentLevel--;
		
			//Finally save the modification done to the object.
			serializedObject.ApplyModifiedProperties();
		}
	}
}
