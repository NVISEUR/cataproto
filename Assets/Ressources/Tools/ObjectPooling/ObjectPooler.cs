﻿using System.Collections.Generic;
using UnityEngine;

namespace Ressources.Tools.ObjectPooling
{
	/// <summary>
	/// Handle the object pooling.
	/// </summary>
	[DisallowMultipleComponent]
	public class ObjectPooler : MonoBehaviour
	{
		/// <summary>
		/// The list of the different pools to create.
		/// </summary>
		[SerializeField] private ObjectPools m_objectPools;

		[SerializeField] private RectTransform m_canvasParent;
		/// <summary>
		/// The dictionary containing all the pools, based on the poolable key.
		/// </summary>
		private Dictionary<string, List<GameObject>> m_pools = new Dictionary<string, List<GameObject>>();
	
		private void Start()
		{
			//Get the transform to create the gameobject as child of this
			Transform l_transform = transform;
		
			//For each objects to pool
			for (int i = 0; i < m_objectPools.Count(); i++)
			{
				//Create the actual pool for this gameobject
				List<GameObject> l_pool = new List<GameObject>();
				//Create the wanted gameobjects, deactivate them and add them to the pool
				for (int j = 0; j < m_objectPools[i].GetAmount();j++)
				{
					GameObject l_obj = Instantiate(m_objectPools[i].GetObject(), m_objectPools[i].IsCanvas() ? m_canvasParent : l_transform);
					l_obj.SetActive(false);
					l_pool.Add(l_obj);
				}

				if (l_pool.Count > 0)
				{
					string l_key = m_objectPools[i].GetObject().name;
					
					//Add the pool to the dictionary using the poolable key
					m_pools.Add(l_key,l_pool);
				}
			}
		}

		/// <summary>
		/// Return a pooled object based on the given key.
		/// </summary>
		/// <param name="p_key">The name of the gameobject we want.</param>
		/// <returns>The first deactivated object in the pool corresponding to the key.</returns>
		public GameObject GetPooledObjectByName(string p_key)
		{
			//Try to get the pool based on the key, return null if the key doesn't link to any pools
			List<GameObject> l_pool;
			m_pools.TryGetValue(p_key, out l_pool);
		
			if (l_pool == null)
				return null;
		
			//For each object in the pool, check if they are active in the hierarchy,
			//If not return it
			for (int i = 0; i < l_pool.Count; i++)
			{
				if (!l_pool[i].activeInHierarchy)
				{
					return l_pool[i];
				}
			}

			//If all the objects are active, return null
			return null;
		}
	}
}