using System;
using System.Collections.Generic;
using UnityEngine;

namespace Ressources.Tools.ObjectPooling
{
    /// <summary>
    /// Represent an object that will be pooled by the pooler.
    /// </summary>
    [Serializable]
    public class ObjectToPool
    {
        /// <summary>
        /// T prefab of the object to pool.
        /// </summary>
        [Tooltip("The prefab of the object you wish to pool.")]
        [SerializeField] private GameObject m_poolObject;
        /// <summary>
        /// The amount of object to pool.
        /// </summary>
        [Tooltip("The amount of object you wish to be in the pool.")]
        [SerializeField] private int m_amount;

        [SerializeField] private bool m_isCanvas;

        public GameObject GetObject()
        {
            return m_poolObject;
        }

        public int GetAmount()
        {
            return m_amount;
        }

        public bool IsCanvas()
        {
            return m_isCanvas;
        }
    }
    
    [CreateAssetMenu(fileName = "ObjectPool", menuName = "Tools/ObjectPools", order = 1)]
    public class ObjectPools : ScriptableObject
    {
        [SerializeField] private List<ObjectToPool> m_objectsToPool;

        public int Count()
        {
            return m_objectsToPool.Count;
        }

        public ObjectToPool this[int key]
        {
            get { return m_objectsToPool[key]; }

            set { m_objectsToPool[key] = value; }
        }
    }
}