﻿using UnityEngine;

namespace Ressources.Tools.StateMachine
{
	[DisallowMultipleComponent]
	public class StateMachine : MonoBehaviour
	{
		/// <summary>
		/// The initial state when the game start.
		/// </summary>
		[SerializeField] private State m_firstState;
		/// <summary>
		/// The current state.
		/// </summary>
		private State m_currentState;
	
		// Use this for initialization
		void Start () 
		{
			ChangeState(m_firstState);
		}
	
		// Update is called once per frame
		void Update ()
		{
			State l_state = m_currentState.DoState();
		
			if(l_state != null)
				ChangeState(l_state);
		}
	
		/// <summary>
		/// Play the exit event, change state and then play the enter event.
		/// </summary>
		/// <param name="state">The new state we are entering.</param>
		public void ChangeState(State state)
		{
			if(m_currentState != null)
				m_currentState.OnStateExit();

			m_currentState = state;
			
			m_currentState.OnStateEnter();
		}
	}
}
