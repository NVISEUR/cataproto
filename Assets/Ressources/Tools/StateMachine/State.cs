using UnityEngine;

namespace Ressources.Tools.StateMachine
{
    /// <summary>
    /// Represent a state of the game at a precise moment.
    /// </summary>
    [DisallowMultipleComponent]
    public abstract class State : MonoBehaviour
    {
        /// <summary>
        /// Handle the behavior that happened when we enter the state.
        /// </summary>
        public abstract void OnStateEnter();
        /// <summary>
        /// Handle the behavior that is executed each frame while we are in the state.
        /// </summary>
        /// <returns>The state we need to move to if any.</returns>
        public abstract State DoState();
        /// <summary>
        /// Handle the behavior that happened when we exit the state.
        /// </summary>
        public abstract void OnStateExit();
    }
}